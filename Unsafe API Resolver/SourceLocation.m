//
//  SourceLocation.m
//  Unsafe API Resolver
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import "SourceLocation.h"

@implementation SourceLocation

- (NSString *)description {
    return [NSString stringWithFormat:@"[%@:%@]", [self.fileName lastPathComponent], self.lineNumber];
}

@end
