//
//  SymbolContainer.h
//  Unsafe API Resolver
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import <Foundation/Foundation.h>

/// The possible types used by the AEWSymbolContainer struct
typedef enum AEWSymbolContainerType {

    AEWSymbolContainerTypeClass,
    AEWSymbolContainerTypeProtocol,
    AEWSymbolContainerTypeCategory,
    AEWSymbolContainerTypeFile,
    AEWSymbolContainerTypeNone

} AEWSymbolContainerType;

/// The container which a symbol was declared in.
@interface SymbolContainer : NSObject

@property (strong) NSString *containerName;
@property (assign) AEWSymbolContainerType type;

@end
