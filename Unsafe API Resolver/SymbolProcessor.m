//
//  SymbolProcessor.m
//  Unsafe API Resolver
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import "SymbolProcessor.h"
#import "Utility.h"

@implementation SymbolProcessor

+ (BOOL)existsInCache:(Symbol *)symbol {
    // TODO: enable caching
    return NO;
}

+ (void)updateCache:(Symbol*)symbol {
    // TODO: enable caching
}

+ (BuildStatus) addSymbol:(Symbol*)symbol
           subprojectRoot:(NSString*)path
                    cache:(NSDictionary*)cache
        criticalClassList:(NSDictionary*)criticalClasses
    symbolsToAddToClasses:(NSMutableDictionary*)postponedSymbolsPerClass {

    if ([SymbolProcessor existsInCache:symbol]) {
        return BuildStatusContinue;
    }
    
    if (symbol.type == AEWSymbolTypeEnum ||
        symbol.type == AEWSymbolTypeExternDefinition ||
        symbol.type == AEWSymbolTypeSubscriptingMethod ) {

        // These are not supported - no automatic resolution
        return BuildStatusContinue;
    }

    // Consider classes?
    BOOL considerClasses = NO;
    if ((symbol.type == AEWSymbolTypeClass || symbol.type == AEWSymbolTypeSubclass)
        && criticalClasses
        && postponedSymbolsPerClass) {
        // 1st stage: only process functions and methods
        return BuildStatusContinue;
    } else if ((symbol.type == AEWSymbolTypeClass || symbol.type == AEWSymbolTypeSubclass)
               && !criticalClasses
               && !postponedSymbolsPerClass) {
        considerClasses = YES;
    }

    // Skip those symbols that are part of a critical class
    NSString *containerName = symbol.container.containerName;
    if (symbol.container.type == AEWSymbolContainerTypeClass
        || symbol.container.type == AEWSymbolContainerTypeCategory) {
        NSDictionary *containerInfo;
        if ((containerInfo = [criticalClasses objectForKey:containerName])) {
            // Hit
            if (containerInfo.allKeys.count == 0) {
                // Container exists already - info comes from the history cache
                considerClasses = YES;
            } else {
                NSMutableDictionary *symbolsForThisClass = [postponedSymbolsPerClass objectForKey:containerName];
                if (!symbolsForThisClass) {
                    symbolsForThisClass = [NSMutableDictionary dictionaryWithCapacity:10];
                    [postponedSymbolsPerClass setObject:symbolsForThisClass forKey:containerName];
                }
                [symbolsForThisClass setObject:symbol forKey:symbol.name];
                return BuildStatusContinue;
            }
        }
    }
    
    // Names/paths of *target* files (not the file templates)
    NSString *headerFileName = [NSString stringWithFormat:@"%@.h", symbol.targetFileName];
    NSString *implFileName = [NSString stringWithFormat:@"%@.m", symbol.targetFileName];

    NSString *headerPath = [path stringByAppendingPathComponent:headerFileName];
    NSString *implPath = [path stringByAppendingPathComponent:implFileName];

    NSString *templatesDir = [[path stringByDeletingLastPathComponent] stringByDeletingLastPathComponent];
    templatesDir = [templatesDir stringByAppendingFormat:@"/Resources/Templates/"];

    // Recompile if code changes have been made to new or existing files
    BOOL recompile = NO;
    // Update the project if new files have been added
    // They need to be added to the subproject before the
    // compilation process can be restarted
    BOOL updateProject = NO;

    // Add files, if necessary; including import management
    NSFileManager *mgr = [NSFileManager defaultManager];
    if (![mgr fileExistsAtPath:headerPath]) {
        // Copy empty template to subproject
        // ../../
        NSString *templateBaseFileName = symbol.templateFileName;

        NSString *templateFilesDir = [templatesDir stringByAppendingPathComponent:@"Files"];
        NSString *fullTemplateHeaderFilePath = [NSString stringWithFormat:@"%@.h", templateBaseFileName];
        NSString *fullTemplateImplFilePath = [NSString stringWithFormat:@"%@.m", templateBaseFileName];

        NSString *headerTemplate = [templateFilesDir stringByAppendingPathComponent:fullTemplateHeaderFilePath];
        NSString *implTemplate = [templateFilesDir stringByAppendingPathComponent:fullTemplateImplFilePath];

        NSError *copyError;
        [mgr copyItemAtPath:headerTemplate
                     toPath:headerPath
                      error:&copyError];
        [mgr copyItemAtPath:implTemplate
                     toPath:implPath
                      error:&copyError];
        if (copyError) {
            NSLog(@"Error copying: %@", copyError);
        }

        // Template files may need some adoption
        if (symbol.type == AEWSymbolTypeProperty ||
            symbol.type == AEWSymbolTypeClassMethod ||
            symbol.type == AEWSymbolTypeInstanceMethod ||
            symbol.type == AEWSymbolTypeClass ||
            symbol.type == AEWSymbolTypeSubclass )
        {

            static NSString *ORIGINAL_CLASS = @"###ORIGINAL_CLASS###";
            static NSString *CONTAINER_CLASS = @"###CONTAINER_CLASS###";

            // Process Header file
            NSString *fileContent = [NSString stringWithContentsOfFile:headerPath
                                                               encoding:NSUTF8StringEncoding
                                                                  error:nil];
            NSString *replacement;
            switch (considerClasses) {
                case YES:
                    if (symbol.type == AEWSymbolTypeClass) {
                        replacement = symbol.name;
                    }
                    if (symbol.type == AEWSymbolTypeSubclass) {
                        replacement = symbol.superclassName;
                    }
                    break;
                case NO:
                    replacement = symbol.container.containerName;
                    break;
                default:
                    break;
            }
            fileContent = [fileContent stringByReplacingOccurrencesOfString:ORIGINAL_CLASS
                                                                 withString:replacement];
            fileContent = [fileContent stringByReplacingOccurrencesOfString:CONTAINER_CLASS
                                                                 withString:symbol.container.containerName];
            [fileContent writeToFile:headerPath
                          atomically:YES
                            encoding:NSUTF8StringEncoding
                               error:nil];

            // Process Impl file
            fileContent = [NSString stringWithContentsOfFile:implPath
                                                    encoding:NSUTF8StringEncoding
                                                       error:nil];
            fileContent = [fileContent stringByReplacingOccurrencesOfString:ORIGINAL_CLASS
                                                                 withString:replacement];
            fileContent = [fileContent stringByReplacingOccurrencesOfString:CONTAINER_CLASS
                                                                 withString:symbol.container.containerName];
            [fileContent writeToFile:implPath
                          atomically:YES
                            encoding:NSUTF8StringEncoding
                               error:nil];
        }

        // Update library header to include new header file
        if (symbol.type == AEWSymbolTypeCFunction
            || symbol.type == AEWSymbolTypeSubclass
            || symbol.type == AEWSymbolTypeClass) {
            
            static NSString *LIB_HEADER_NAME = @"SHManagedCompatibilityLibrary.h";
            NSString *libHeaderPath = [path stringByAppendingPathComponent:LIB_HEADER_NAME];
            NSString *fullImport = [@"SHManagedCompatibilityLibrary/" stringByAppendingString:headerFileName];
            [Utility addImportForFiles:@[fullImport]
                                toFile:libHeaderPath
                       asSystemHeaders:NO
                   checkForIndentation:YES];
        }
        
        recompile = YES;
        updateProject = YES; // not only changed existing files, but also added new ones

    } // End of adding new files

    // At this point, either a previously created file or an empty template exists
    // Symbol already defined in file?
    NSString *fileContent = [NSString stringWithContentsOfFile:headerPath
                                                      encoding:NSUTF8StringEncoding
                                                         error:nil];

    NSArray *methodsToAdd = (considerClasses)? symbol.subSymbols.allValues : @[symbol];
    for (Symbol *method in methodsToAdd) {
        // Never overwrite alloc/allocWithZone:
        // Same goes for performSelector...
        if ([method.name hasPrefix:@"alloc"]
            || [method.name hasPrefix:@"performSelector"]) {
            // Skip to next method
            continue;
        }
        
        NSRange symbolLoc = [fileContent rangeOfString:[method searchString:(!considerClasses)]];
        if (symbolLoc.location == NSNotFound) {
            NSString *snippetsDir = [templatesDir stringByAppendingPathComponent:@"CodeSnippets"];

            // New symbol - add it
            // Add declaration to header
            NSString *doc = [method headerDocumentation:snippetsDir];
            NSString *decl = [[method declaration:(!considerClasses)] stringByAppendingString:@";\n\n"];
            decl = [doc stringByAppendingFormat:@"\n%@", decl];
            if (method.type == AEWSymbolTypeCFunction) {
                // Just append at the end of the file...
                NSFileHandle *headerFileHandle = [NSFileHandle fileHandleForUpdatingAtPath:headerPath];
                [headerFileHandle seekToEndOfFile];
                [headerFileHandle writeData:[decl dataUsingEncoding:NSUTF8StringEncoding]];
            } else {
                // Insert within correct location: below @interface line
                NSString *fileContent = [NSString stringWithContentsOfFile:headerPath
                                                                  encoding:NSUTF8StringEncoding
                                                                     error:nil];
                NSRange interfaceRange = [fileContent rangeOfString:@"@interface"];
                NSRange newLineRange = [fileContent rangeOfString:@"\n"
                                                          options:NSLiteralSearch
                                                            range:NSMakeRange(interfaceRange.location,
                                                                              fileContent.length-interfaceRange.location-1)];
                newLineRange.location += 2; // leave some space
                newLineRange.length = 0; // do not overwrite anything

                fileContent = [fileContent stringByReplacingCharactersInRange:newLineRange
                                                                   withString:decl];
                [fileContent writeToFile:headerPath
                              atomically:YES
                                encoding:NSUTF8StringEncoding
                                   error:nil];
            }

            // Add definition to implementation file
            NSString *impl = [[method definition:snippetsDir
                                       usePrefix:(!considerClasses)]
                              stringByAppendingString:@"\n\n"];
            
            if (method.type == AEWSymbolTypeCFunction) {
                NSFileHandle *implFileHandle = [NSFileHandle fileHandleForUpdatingAtPath:implPath];
                [implFileHandle seekToEndOfFile];
                [implFileHandle writeData:[impl dataUsingEncoding:NSUTF8StringEncoding]];
                [implFileHandle closeFile];
            } else {
                // Insert within correct location: below #pragma mark -
                NSString* implFileContent = [NSString stringWithContentsOfFile:implPath
                                                                      encoding:NSUTF8StringEncoding
                                                                         error:nil];

                NSString *pragma = @"#pragma mark - Generated methods";
                if (considerClasses && method.declarationIsUncritical) {
                    // These methods are sort of optional:
                    // They are not declared in the critical
                    // class, but the critical class may change
                    // their initial implementation, so they
                    // get their own section
                    pragma = @"#pragma mark - Methods that";
                }
                NSRange interfaceRange = [implFileContent rangeOfString:pragma];
                NSRange newLineRange = [implFileContent rangeOfString:@"\n"
                                                              options:NSLiteralSearch
                                                                range:NSMakeRange(interfaceRange.location,
                                                                                  implFileContent.length-interfaceRange.location-1)];
                newLineRange.location += 2; // leave some space
                newLineRange.length = 0; // do not overwrite anything
                implFileContent = [implFileContent stringByReplacingCharactersInRange:newLineRange
                                                                           withString:impl];

                if (!considerClasses) {
                    // Enable dynamic method resolution
                    // Skip 'r': Either managed or original selector
                    NSString *searchStart = (method.type == AEWSymbolTypeClassMethod)?
                    @"esolveClassMethod":
                    @"esolveInstanceMethod";
                    NSString *inverseMethod = (method.type == AEWSymbolTypeClassMethod)?
                    @"esolveInstanceMethod":
                    @"esolveClassMethod";

                    NSRange startRange = [implFileContent rangeOfString:searchStart];
                    NSRange endRange = [implFileContent rangeOfString:inverseMethod];

                    if (startRange.location == NSNotFound) {
                        // This should never happen, but the app
                        // should not crash...
                        NSLog(@"Error: Range of '%@' not found in: '%@' (%@)",
                              searchStart,
                              implFileContent,
                              symbol);
                        return BuildStatusContinue;
                    }

                    if (endRange.location < startRange.location) {
                        // Prevent searching in the wrong implementation block
                        // If the wrong block is above the correct block,
                        endRange.location = implFileContent.length;
                        endRange.length = 0;
                    }

                    /*
                     *  Start from here: look for if(...
                     *                            (1)...NO_SELECTOR_YET: first method, replace NO_SELECTOR_YET
                     *                            (2)...sel ==... append another line
                     */
                    NSRange ifStmtRange = [implFileContent rangeOfString:@"if (NO_SELECTOR_YET)"
                                                                 options:NSLiteralSearch
                                                                   range:NSMakeRange(startRange.location,
                                                                                     endRange.location-startRange.location)];

                    NSRange replacementRange;
                    BOOL isFirst = NO;
                    if (ifStmtRange.location == NSNotFound) {
                        // Case (2)
                        ifStmtRange = [implFileContent rangeOfString:@"if (sel =="
                                                             options:NSLiteralSearch
                                                               range:NSMakeRange(startRange.location,
                                                                                 implFileContent.length-startRange.location-1)];

                        replacementRange = ifStmtRange;
                        replacementRange.location += 4; // jump to first 'sel ==...'
                        replacementRange.length = 0; // leave everything in place

                    } else {
                        // Case (1)
                        isFirst = YES;
                        replacementRange = ifStmtRange;
                        replacementRange.location += 4; // skip 'if ('
                        replacementRange.length = 15; // cover 'NO_SELECTOR_YET'
                    }

                    // Insert new selector
                    NSString *linebreak = (isFirst)?
                    @"":
                    @" ||\n        ";
                    NSString *newCode = [NSString stringWithFormat:@"sel == @selector(%@)%@", method.name, linebreak];
                    implFileContent = [implFileContent stringByReplacingCharactersInRange:replacementRange
                                                                               withString:newCode];
                }
                [implFileContent writeToFile:implPath
                                  atomically:YES
                                    encoding:NSUTF8StringEncoding
                                       error:nil];
            }
            recompile = YES;

        } else {

            // Update the documentation
            static NSString *refString = @"References:";
            NSRange fromHereToStart = NSMakeRange(0, symbolLoc.location);

            // Backwardssearch: Make sure to update the doc above *this* symbol
            NSRange refRange = [fileContent rangeOfString:refString
                                                  options:NSBackwardsSearch
                                                    range:fromHereToStart];
            NSUInteger length = symbolLoc.location - refRange.location;
            NSRange refToNextNewline = NSMakeRange(refRange.location, length);
            NSRange newLineRange = [fileContent rangeOfString:@" *  \n"
                                                      options:NSLiteralSearch
                                                        range:refToNextNewline];
            NSUInteger startOfFirstLineAfterRef = refRange.location + refRange.length + @"\n".length;
            NSRange betweenRefAndNewLine = NSMakeRange(startOfFirstLineAfterRef,
                                                       newLineRange.location-startOfFirstLineAfterRef);
            
            fileContent = [fileContent stringByReplacingCharactersInRange:betweenRefAndNewLine
                                                               withString:[NSString stringWithFormat:@"%@",
                                                                           [method headerDocumentationReferences]]];
            [fileContent writeToFile:headerPath
                          atomically:YES
                            encoding:NSUTF8StringEncoding
                               error:nil];
        }
    }

    [SymbolProcessor updateCache:symbol];
    
    if (updateProject) {
        return BuildStatusRequiresUpdatingProject;
    } else if (recompile) {
        return BuildStatusRequiresRecompilation;
    }
    return BuildStatusContinue;
}

@end
