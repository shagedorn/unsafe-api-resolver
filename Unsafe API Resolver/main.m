//
//  main.m
//  Unsafe API Resolver
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import <Foundation/Foundation.h>

#import "Utility.h"
#import "ScriptExecution.h"
#import "Symbol.h"
#import "SymbolProcessor.h"

int main(int argc, const char * argv[])
{

  @autoreleasepool {

#pragma mark - Early Termination
      /*
       *    (1) Early termination
       *
       *    Ideally: Abort if...
       *        (1.1) ...Clang was not run with '-export-unsafe-api-usage' flag
       *        (1.2) ...there's no 'APISymbolsExport' directory or PLIST file
       *        (1.3) ...the PLIST has not been touched since the last time the tool ran successfully
       *
       */

      // Collect information about the environment
      NSDictionary *environment = [[NSProcessInfo processInfo] environment];

      // Flag info
      static NSString *OTHER_C_FLAGS_KEY = @"OTHER_CFLAGS";
      NSString *otherCFlags = [environment objectForKey:OTHER_C_FLAGS_KEY];
      static NSString *EXPORT_FLAG = @"-export-unsafe-api-usage";
      NSRange flagRange = [otherCFlags rangeOfString:EXPORT_FLAG];

      // (1.1)
      if (flagRange.location == NSNotFound) {
          NSLog(@"Finished: Export flag ('-export-unsafe-api-usage') not set for Clang.");
          return 0;
      }

      // PLIST
      static NSString *EXPORT_DIR_KEY = @"TARGET_TEMP_DIR";
      NSString *exportDirectory = [[environment objectForKey:EXPORT_DIR_KEY] stringByAppendingPathComponent:@"APISymbolsExport"];
      static NSString *SYMBOLS_FILE_NAME = @"symbols.plist";
      NSString *symbolsFilePath = [exportDirectory stringByAppendingPathComponent:SYMBOLS_FILE_NAME];

      // (1.2)
      NSDictionary *symbolsDictionary = [NSDictionary dictionaryWithContentsOfFile:symbolsFilePath];
      if (!symbolsDictionary) {
          NSLog(@"Finished: No PLIST found under %@.", exportDirectory);
          return 0;
      }

      // Cache Info
      static NSString *CACHE_FILE_NAME = @"cache.plist";
      static NSString *TOKEN_KEY = @"symbolsToken";
      NSString *cacheFilePath = [exportDirectory stringByAppendingString:CACHE_FILE_NAME];
      NSDictionary *cacheDictionary = [NSDictionary dictionaryWithContentsOfFile:cacheFilePath];
      NSString *symbolsToken = [cacheDictionary objectForKey:TOKEN_KEY];
      NSString *currentToken = [Utility generateTokenFromFile:symbolsFilePath];

      // (1.3)
      if (cacheDictionary && [symbolsToken isEqualToString:currentToken]) {
          NSLog(@"Finished: No changes.");
          return 0;
      }

      // Either the cache is empty or its content has changed
      NSMutableDictionary *mutableCacheDict = [cacheDictionary mutableCopy];
      if (!mutableCacheDict) {
          // First time after clean
          mutableCacheDict = [NSMutableDictionary new];
      }

#pragma mark - Subproject check
      /*
       *    (2) Check: Does subproject exist?
       *            NO: Warning/abort
       */
      // TODO: Workspace support
      static NSString *PROJ_KEY = @"PROJECT_FILE_PATH";
      NSString *projectName = [environment objectForKey:PROJ_KEY];
      static NSString *PROJECT_FILE_NAME = @"project.pbxproj";
      NSString *projectInfosFileName = [projectName stringByAppendingPathComponent:PROJECT_FILE_NAME];
      NSDictionary *projectInfos = [NSDictionary dictionaryWithContentsOfFile:projectInfosFileName];

      // 1st Check: Does project reference exist?
      static NSString *LIB_PROJECT_NAME = @"SHManagedCompatibilityLibrary";
      // 2nd Check: Does the library product exist?
      static NSString *LIB_PRODUCT_NAME = @"libSHManagedCompatibilityLibrary.a";
      
      NSString *libFileName = [NSString stringWithFormat:@"%@.xcodeproj", LIB_PROJECT_NAME];
      NSArray *allProjectObjects = [[projectInfos objectForKey:@"objects"] allValues];

      BOOL foundProject = NO;
      BOOL foundProduct = NO;
      NSString *name;
      NSString *path;
      for (NSDictionary *object in allProjectObjects) {
          name = [object objectForKey:@"name"];
          if ([name isEqualToString:libFileName]) {
              foundProject = YES;
          }

          path = [object objectForKey:@"path"];
          if ([path isEqualToString:LIB_PRODUCT_NAME]) {
              foundProduct = YES;
          }
          // No more unneccessary work...
          if (foundProduct && foundProject)
              break;
      }
      if (!foundProject) {
          NSLog(@"Abort: Library subproject not found.");
          return 0;
      } else if (!foundProduct) {
          NSLog(@"Abort: Library product not found.");
          return 0;
      }
      
      // Subproject exists

#pragma mark - Code Generation
      /*
       *    (3) Generate code
       */

      static NSString *SUPER_PROJ_DIR_KEY = @"PROJECT_DIR";
      NSString *superProjectDir = [environment objectForKey:SUPER_PROJ_DIR_KEY];
      NSString *subProjectDir = [superProjectDir stringByAppendingFormat:@"/%@/%@/", LIB_PROJECT_NAME, LIB_PROJECT_NAME];
      BuildStatus status = BuildStatusContinue;

      /*
       *    Get a list of all critical classes first
       *
       *    For each method:
       *        (1) Part of critical class? Add to proxy class (later)
       *        (2) Else: Resolve with Categories and resolveInstance/ClassMethod:
       */
      __block NSMutableDictionary *allCriticalClasses = [NSMutableDictionary dictionary];
      __block NSString *classTypePrefix = [NSString stringWithFormat:@"%d", (int)AEWSymbolTypeClass];
      __block NSString *subclassTypePrefix = [NSString stringWithFormat:@"%d", (int)AEWSymbolTypeSubclass];
      NSString *classHistoryFilePath = [subProjectDir stringByAppendingPathExtension:@"CriticalClassHistory.plist"];
      __block NSMutableDictionary *classHistory = [[NSDictionary dictionaryWithContentsOfFile:classHistoryFilePath] mutableCopy];
      __block BOOL classHistoryHasChanged = NO;
      [symbolsDictionary enumerateKeysAndObjectsUsingBlock:^(NSString *key,
                                                             NSDictionary *symbol,
                                                             BOOL *stop) {
          if ([key hasPrefix:classTypePrefix] || [key hasPrefix:subclassTypePrefix]) {
              NSString *className = [symbol objectForKey:NAME_KEY];
              [allCriticalClasses setObject:symbol forKey:className];

              // Save this information for later - critical classes
              // do not remain in source code if you apply the FixIt

              // Lazy initialisation
              if (!classHistory) {
                  classHistory = [NSMutableDictionary dictionary];
              }

              // Save for later
              if (![classHistory objectForKey:className]) {
                  [classHistory setObject:@(1) forKey:className];
                  classHistoryHasChanged = YES;
              }
          }
      }];
      if (classHistoryHasChanged) {
          [classHistory writeToFile:classHistoryFilePath
                         atomically:YES];
      }
      [classHistory enumerateKeysAndObjectsUsingBlock:^(NSString *key, id obj, BOOL *stop) {
          // Add history to current critical classes list
          if (![allCriticalClasses objectForKey:key]) {
              [allCriticalClasses setObject:@{} forKey:key];
          }
      }];
      
      /*
       *    Process functions, class/instance methods, and properties
       *
       */
      NSMutableDictionary *symbolsToAddToProxyClasses = [NSMutableDictionary dictionary];
      for (NSDictionary *symbolInfo in symbolsDictionary.allValues) {

          Symbol *thisSymbol = [Symbol symbolFromDictionary:symbolInfo];
          BuildStatus oneStatus = [SymbolProcessor addSymbol:thisSymbol
                                              subprojectRoot:subProjectDir
                                                       cache:cacheDictionary
                                           criticalClassList:allCriticalClasses
                                       symbolsToAddToClasses:symbolsToAddToProxyClasses];
          if (oneStatus > status) {
              // Keep the most severe status
              status = oneStatus;
          }
      }


      /*
       *    Process classes
       */
      for (NSString *classKey in allCriticalClasses.allKeys) {
          NSDictionary *classSymbol = [allCriticalClasses objectForKey:classKey];
          if (classSymbol.allKeys.count == 0) {
              // Just a class from the history - do not process it again
              continue;
          }
          Symbol *symbolObject = [Symbol symbolFromDictionary:classSymbol];

          // Get all the methods that need to be added to this class
          NSDictionary *allMethods = [symbolsToAddToProxyClasses objectForKey:classKey];
          symbolObject.subSymbols = allMethods;
          BuildStatus oneStatus = [SymbolProcessor addSymbol:symbolObject
                                              subprojectRoot:subProjectDir
                                                       cache:cacheDictionary
                                           criticalClassList:nil
                                       symbolsToAddToClasses:nil];
          if (oneStatus > status) {
              // Keep the most severe status
              status = oneStatus;
          }
      }

#pragma mark - Termination
      /*
       *    (4) Terminated successfully
       *
       *    Any code changes?   YES: Restart build process
       *                        NO:  Continue
       *    Anyhow:             Remember the PLIST that was used for this run
       *
       */

      [mutableCacheDict setObject:currentToken forKey:TOKEN_KEY];
      [mutableCacheDict writeToFile:cacheFilePath atomically:YES];

      if (status >= BuildStatusRequiresRecompilation) {

          // Make sure to keep frameworks up to date
          // Compare: Frameworks of target project == frameworks of library project?
          NSString *subProjectPath = [[subProjectDir stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"SHManagedCompatibilityLibrary.xcodeproj/project.pbxproj"];
          NSDictionary *subProjectInfos = [NSDictionary dictionaryWithContentsOfFile:subProjectPath];

          NSDictionary *superProjectFws = [Utility frameworksFromProject:projectInfos];
          NSDictionary *subProjectFws = [Utility frameworksFromProject:subProjectInfos];
          NSArray *newFrameworks = [Utility returnElementsFrom:superProjectFws
                                                notContainedIn:subProjectFws];
          
          if (newFrameworks.count > 0) {
              // Adds frameworks to (sub)project file and to its link phase
              [ScriptExecution executeScript:PythonScriptAddFrameworks withArguments:newFrameworks];

              // Update imports: PCH file AND Lib collection header
              // The latter is important to fix build failes for plain C (header) files
              NSString *pchFilePath = [subProjectDir stringByAppendingPathComponent:@"SHManagedCompatibilityLibrary-Prefix.pch"];
              NSString *libHeaderPath = [subProjectDir stringByAppendingPathComponent:@"SHManagedCompatibilityLibrary.h"];

              // Generate import file names
              NSMutableArray *names = [NSMutableArray arrayWithCapacity:newFrameworks.count];
              NSString *frameworkHeaderName;
              for (NSString *frameworkPath in newFrameworks) {
                  frameworkHeaderName = [[frameworkPath lastPathComponent] stringByDeletingPathExtension];
                  frameworkHeaderName = [NSString stringWithFormat:@"%@/%@.h", frameworkHeaderName, frameworkHeaderName];
                  [names addObject:frameworkHeaderName];
              }

              // Add to pch
              [Utility addImportForFiles:names
                                  toFile:pchFilePath
                         asSystemHeaders:YES
                     checkForIndentation:YES];

              // Add the same imports to the framework header
              [Utility addImportForFiles:names
                                  toFile:libHeaderPath
                         asSystemHeaders:YES
                     checkForIndentation:YES];
          }
      }

      if (status == BuildStatusRequiresUpdatingProject) {
          // Add files to subproject
          [ScriptExecution executeScript:PythonScriptAddFiles withArguments:nil];
      }

      // If files have been added, always restart compilation
      if (status >= BuildStatusRequiresRecompilation) {

          // Defaults to 'Build'
          static NSString *ACTION_KEY = @"ACTION";
          NSString *actionToRestart = [[environment objectForKey:ACTION_KEY] capitalizedString];
          // TODO: what about workspaces? --> pass name without extension?
          // --> go through windows and check their documents
          NSString *projectFileName = [projectName lastPathComponent];

          NSLog(@"Finished: Restart build process ('%@').", actionToRestart);

          // Arguments: [0]: Name of the app to build [1]: Build action (Run, Analyze,...)
          [ScriptExecution executeScript:AppleScriptScriptRebuild withArguments:@[projectFileName, actionToRestart]];
          return 0;

      } else {
          NSLog(@"Finished: No changes that require restarting the build process.");
      }

  }
    return 0;
}

