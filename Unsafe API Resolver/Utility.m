//
//  Utility.m
//  Unsafe API Resolver
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import "Utility.h"

@implementation Utility

+ (NSString *)generateTokenFromFile:(NSString *)filePath {

    NSFileManager *mgr = [NSFileManager defaultManager];
    NSError *error;
    NSDictionary *fileAttributes = [mgr attributesOfItemAtPath:filePath error:&error];

    if (error) {
        // Do not use the cache
        return nil;
    }

    NSString *modDate = [fileAttributes objectForKey:NSFileModificationDate];
    NSString *size = [fileAttributes objectForKey:NSFileSize];
    return [NSString stringWithFormat:@"%@#%@", modDate, size];
}

+ (NSDictionary *) frameworksFromProject:(NSDictionary*)contentsOfProjectFile {

    NSMutableDictionary *fw = [NSMutableDictionary dictionaryWithCapacity:20];
    NSDictionary *objects = [contentsOfProjectFile objectForKey:@"objects"];

    [objects enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSDictionary *obj, BOOL *stop) {
        if ([[obj objectForKey:@"lastKnownFileType"] isEqualToString:@"wrapper.framework"]) {
            [fw setObject:[obj objectForKey:@"path"] forKey:[obj objectForKey:@"name"]];
        }
    }];

    return fw;
}

+ (NSArray *)returnElementsFrom:(NSDictionary *)superDict
                 notContainedIn:(NSDictionary *)subDict {

    NSMutableArray *elements = [NSMutableArray arrayWithCapacity:superDict.count];
    __block id testObject;
    [superDict enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *value, BOOL *stop) {
        testObject = [subDict objectForKey:key];
        if (!testObject) {
            // subDict does not have a value for this key
            [elements addObject:value];
        }
    }];

    return elements;
}

+ (void)addImportForFiles:(NSArray *)fileNames
                   toFile:(NSString *)targetFilePath
          asSystemHeaders:(BOOL)systemHeaders
      checkForIndentation:(BOOL)setIdentation {

    // Find existing imports (by default: Foundation)
    NSString *targetFileContent = [NSString stringWithContentsOfFile:targetFilePath
                                                      encoding:NSUTF8StringEncoding
                                                         error:nil];
    char openingChar = (systemHeaders)? '<' : '\"';
    char closingChar = (systemHeaders)? '>' : '\"';

    // Requires existing import of same type
    NSString *existingImportTemplate = [NSString stringWithFormat:@"#import %c", openingChar];
    NSRange anyExistingImportRange = [targetFileContent rangeOfString:existingImportTemplate];
    if (anyExistingImportRange.location == NSNotFound) {
        NSString *typeName = (systemHeaders)? @"systen":@"user";
        NSLog(@"No existing %@ header found in %@. %@ were not added.",
              typeName,
              targetFilePath,
              fileNames);
        return;
    }

    NSString *tab = @"";
    if (setIdentation) {

        // Find out tab space of existing import
        anyExistingImportRange.length = 0; // do not replace anything
        NSRange searchRange = NSMakeRange(0, anyExistingImportRange.location);

        // Find out tab space used
        NSRange tabRange = [targetFileContent rangeOfString:@"\n"
                                                    options:NSBackwardsSearch
                                                      range:searchRange];
        // excluding newline
        NSUInteger tabSize = anyExistingImportRange.location -  tabRange.location - 1;
        if (tabSize > 0) {
            tab = [NSString stringWithFormat:@"%*s", (int)tabSize, " "];
        }
    }

    // Generate import directives
    NSMutableString *imports = [NSMutableString string];
    NSString *import;
    for (NSString *fileName in fileNames) {
        import = [NSString stringWithFormat:@"#import %c%@%c\n%@",
                  openingChar,
                  fileName,
                  closingChar,
                  tab];
        [imports appendString:import];
    }

    // Insert imports
    targetFileContent = [targetFileContent stringByReplacingCharactersInRange:anyExistingImportRange
                                                                   withString:imports];

    // Write back
    NSError *error;
    [targetFileContent writeToFile:targetFilePath
                  atomically:YES
                    encoding:NSUTF8StringEncoding
                       error:&error];
}

+ (NSString*) signatureFromSelector:(NSString*)selAsString
                      andParameters:(NSArray*)params
                       includeTypes:(BOOL)withTypes {
    NSRange colonRange = [selAsString rangeOfString:@":"];
    int count = 0;
    NSString *param;
    NSString *space;
    while (colonRange.location != NSNotFound) {

        colonRange.location++; // insert after colon
        colonRange.length = 0; // no overwriting
        NSString *paramName = params[count][1];
        space = (count == (params.count-1))? @"":@" ";

        if (withTypes) {
            NSString *paramType = params[count][0];
            param = [NSString stringWithFormat:@"(%@)%@%@", paramType, paramName, space];
        } else {
            param = [paramName stringByAppendingString:space];
        }

        selAsString = [selAsString stringByReplacingCharactersInRange:colonRange
                                                           withString:param];

        // search for next colon
        colonRange = [selAsString rangeOfString:@":"
                                        options:NSLiteralSearch
                                          range:NSMakeRange(colonRange.location,
                                                            selAsString.length-colonRange.location)];

        count++;
    }
    
    return selAsString;
}

+ (NSString*) signatureFromSelector:(NSString*)selAsString
                      andParameters:(NSArray*)params {
    return [Utility signatureFromSelector:selAsString
                            andParameters:params
                             includeTypes:YES];
}

+ (NSString *)selector:(NSString *)sel withArgs:(NSArray *)arguments {
    return [Utility signatureFromSelector:sel
                            andParameters:arguments
                             includeTypes:NO];
}

@end
