//
//  SymbolProcessor.h
//  Unsafe API Resolver
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import <Foundation/Foundation.h>
#import "Symbol.h"

/**
 *  Keep the ascending order:
 *  low:    No interruption
 *  high:   High interruption
 */
typedef enum BuildStatus {

    BuildStatusContinue,
    BuildStatusRequiresRecompilation,
    BuildStatusRequiresUpdatingProject

} BuildStatus;

@interface SymbolProcessor : NSObject

/**
 *  Adds methods and functions, if they have not been declared
 *  before. Only the header is checked to see whether a symbol
 *  has been added before.
 *
 *  If you want to add classes, set the parameters
 *  'criticalClassList' and 'symbolsToAddToClasses' to nil.
 *
 *  param criticalClassList: A list of all critical classes.
 *                      Only resolve symbol if it is not part
 *                      of a critical class.
 *
 *  @return:    Information whether the build can continue
 *              normally, restarted, or files need to be added
 *              to the project.
 */
+ (BuildStatus) addSymbol:(Symbol*)symbol
           subprojectRoot:(NSString*)path
                    cache:(NSDictionary*)cache
        criticalClassList:(NSDictionary*)criticalClasses
    symbolsToAddToClasses:(NSMutableDictionary*)postponedSymbolsPerClass;

@end
