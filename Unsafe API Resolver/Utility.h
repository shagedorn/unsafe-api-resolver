//
//  Utility.h
//  Unsafe API Resolver
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject


/**
 *  Generate a token from a file at the fiven path. The token
 *  should change whenever the file was changed.
 *
 *  @param filePath: The absolute path to the file.
 *  @return:    The token.
 */
+ (NSString*) generateTokenFromFile:(NSString*)filePath;

/**
 *  Returns a list of all frameworks that are currently linked
 *  from the given project.
 */
+ (NSDictionary *) frameworksFromProject:(NSDictionary*)contentsOfProjectFile;

/**
 *  For every key of superDict, see if it exists in
 *  subDict. If not, the value in superDict is put
 *  in the returned array.
 */
+ (NSArray *) returnElementsFrom:(NSDictionary*)superDict
                  notContainedIn:(NSDictionary*)subDict;

/**
 *  Add import directive to a file. Group by system/user
 *  headers.
 *
 *  Requires existing import of the same type.
 */
+ (void) addImportForFiles:(NSArray*)fileNames
                    toFile:(NSString *)targetFilePath
            asSystemHeaders:(BOOL)systemHeaders
       checkForIndentation:(BOOL)setIdentation;

/**
 *  Build the signature (without the return type)
 *  from a given selector and the arguments.
 */
+ (NSString*) signatureFromSelector:(NSString*)selAsString
                      andParameters:(NSArray*)params;

/**
 *  Build a selector with its arguments (no argument types).
 */
+ (NSString*) selector:(NSString*)sel
              withArgs:(NSArray*)arguments;

@end
