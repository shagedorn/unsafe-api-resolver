//
//  ScriptExecution.h
//  Unsafe API Resolver
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import <Foundation/Foundation.h>

/**
 *  A list of currently supported scripts
 */
typedef enum ScriptType {
    AppleScriptScriptRebuild,
    PythonScriptAddFiles,
    PythonScriptAddFrameworks
} ScriptType;

/**
 *  This class supports the execution of AppleScripts with command line arguments.
 */
@interface ScriptExecution : NSObject

/**
 *  Execute a script with a given name. Arguments are optional (depending on the actual script).
 */
+ (void) executeScript:(ScriptType)type withArguments:(NSArray*)args;

@end
