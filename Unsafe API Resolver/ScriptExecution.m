//
//  ScriptExecution.m
//  Unsafe API Resolver
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import "ScriptExecution.h"

/*
 *  Relative path to folder with scripts
 *
 *  Relative to the project that uses this tool (target project). An
 *  installer should copy the script to the correct location within
 *  the target project.
 */
static NSString *SCRIPTS_FOLDER_PATH = @"./Resources/Scripts/";

@implementation ScriptExecution

+ (void)executeScript:(ScriptType)type
        withArguments:(NSArray *)args {

    NSString *scriptPath = [ScriptExecution scriptPathForType:type];
    NSError *scriptLoadingError;
    NSDictionary *errors;

    if (type == AppleScriptScriptRebuild) {
        // Only AppleScripts are run from source code as string
        // Python scripts are run from files directly
        NSString *source = [NSString stringWithContentsOfFile:scriptPath
                                                     encoding:NSUTF8StringEncoding
                                                        error:&scriptLoadingError];
        source = [ScriptExecution modifiedSource:source
                                         forType:type
                                         andArgs:args];
        
        // Restart build process via AppleScript
        NSAppleScript *script = [[NSAppleScript alloc] initWithSource:source];
        [script compileAndReturnError:&errors];
        [script executeAndReturnError:&errors];
        
    } else if (type == PythonScriptAddFiles || type == PythonScriptAddFrameworks) {

        // Run python script
        NSString *currentDir = [NSFileManager defaultManager].currentDirectoryPath;
        NSString *absoluteScriptPath = [currentDir stringByAppendingPathComponent:scriptPath];

        NSTask *scriptTask = [[NSTask alloc] init];
        NSMutableArray *allArgs = [NSMutableArray arrayWithObjects:absoluteScriptPath,[currentDir stringByAppendingPathComponent:@"SHManagedCompatibilityLibrary"], nil];
        if (args.count > 0) {
            [allArgs addObjectsFromArray:args];
        }
        scriptTask.arguments = allArgs;
        scriptTask.launchPath = @"/usr/bin/python";

        [scriptTask launch];
        [scriptTask waitUntilExit];

    }

    if (scriptLoadingError) {
        NSLog(@"Error loading script of type %d: %@", type, scriptLoadingError);
        return;
    }
    if (errors) {
        NSLog(@"Errors during the compilation/execution of script of type %d: %@", type, errors);
    }
    
}

/**
 *  Obtain the absolute path to the script of a given type.
 */
+ (NSString*) scriptPathForType:(ScriptType)type {
    NSString *name;
    switch (type) {
        case AppleScriptScriptRebuild:
            name = @"xcode_restart_build.applescript";
            break;
        case PythonScriptAddFiles:
            name = @"add_file_to_project.py";
            break;
        case PythonScriptAddFrameworks:
            name = @"add_frameworks_to_project.py";
            break;
        default:
            break;
    }
    name = [SCRIPTS_FOLDER_PATH stringByAppendingPathComponent:name];
    return name;
}

/**
 *  It is not trivial to pass arguments to AppleScripts from
 *  ObjC. Therefore, we directly modify the scripts by
 *  replacing placeholder strings. These strings are to be found
 *  at the very beginning of the respective scripts.
 */
+ (NSString*) modifiedSource:(NSString*)template
                     forType:(ScriptType)type
                     andArgs:(NSArray*)args {
    
    switch (type) {
        case AppleScriptScriptRebuild: {
            static NSString *PROJECT_NAME = @"###PROJECT_NAME###";
            static NSString *BUILD_ACTION = @"###BUILD_ACTION###";

            template = [template stringByReplacingOccurrencesOfString:PROJECT_NAME
                                                           withString:args[0]];
            template = [template stringByReplacingOccurrencesOfString:BUILD_ACTION
                                                           withString:args[1]];
            
            break;
        }
        case PythonScriptAddFiles: {
            // No modifications - we use command line args instead
            break;
        }
        default:
            break;
    }

    return template;
}

@end
