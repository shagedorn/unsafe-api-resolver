//
//  SourceLocation.h
//  Unsafe API Resolver
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import <Foundation/Foundation.h>

/// The source code location where a symbol has been found
@interface SourceLocation : NSObject

@property (strong) NSString *fileName;
@property (strong) NSNumber *lineNumber;

@end
