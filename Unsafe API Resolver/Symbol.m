//
//  Symbol.m
//  Unsafe API Resolver
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import "Symbol.h"
#import "Utility.h"

#pragma mark - Private Interface

static NSDictionary *_snippets = nil;

@interface Symbol ()

- (NSDictionary*) createLocationDict;

@end

#pragma mark - Implementation

@implementation Symbol

#pragma mark - Factory

+ (Symbol *)symbolFromDictionary:(NSDictionary*)symbolInfo {

    Symbol *thisSymbol = [Symbol new];
    thisSymbol.name = [symbolInfo objectForKey:NAME_KEY];

    NSString *typeString = [symbolInfo objectForKey:TYPE_KEY];
    thisSymbol.type = [Symbol typeForSymbolName:typeString];

    NSString *versionString = [symbolInfo objectForKey:MIN_SDK_KEY];
    AEWiOSVersion version;
    NSArray *components = [versionString componentsSeparatedByString:@"."];
    version.major = [[components objectAtIndex:0] intValue];
    version.minor = [[components objectAtIndex:1] intValue];
    thisSymbol.minSDKVersion = version;

    SymbolContainer *container = [SymbolContainer new];
    NSDictionary *containerDict = [symbolInfo objectForKey:CONTAINER_KEY];
    NSString *containerTypeString = [containerDict objectForKey:CONTAINER_TYPE_KEY];
    AEWSymbolContainerType containerType = [Symbol typeForContainerTypeName:containerTypeString];
    container.containerName = [containerDict objectForKey:CONTAINER_NAME_KEY];
    container.type = containerType;
    thisSymbol.container = container;

    NSArray *locations = [symbolInfo objectForKey:LOCATIONS_KEY];
    NSMutableArray *myLocations = [NSMutableArray arrayWithCapacity:locations.count];
    for (NSDictionary *oneLocation in locations) {
        SourceLocation *loc = [SourceLocation new];
        loc.lineNumber = [oneLocation objectForKey:LOCATION_LINE_KEY];
        loc.fileName = [oneLocation objectForKey:LOCATION_FILE_KEY];
        [myLocations addObject:loc];
    }
    thisSymbol.locations = [NSArray arrayWithArray:myLocations];

    thisSymbol.superclassName = [symbolInfo objectForKey:SUPERCLASS_KEY];
    thisSymbol.returnType = [symbolInfo objectForKey:RETURN_TYPE_KEY];

    thisSymbol.params = [symbolInfo objectForKey:PARAMS_KEY];
    thisSymbol.declarationIsUncritical = [[symbolInfo objectForKey:UNCRITICAL_DECL_KEY] boolValue];

    return thisSymbol;
}

#pragma mark - Debug Helper

- (NSString *)description {
    return [NSString stringWithFormat:@"Symbol '%@' (%@)", self.name, [Symbol nameForSymbolType:self.type]];
}

- (NSString *)debugDescription {
    return [NSString stringWithFormat:@"\n\nExport Symbol (%@) '%@':\nReferenced at: [%@]\nType: %@\nSince: %d.%d\nDeclared in: %@ (%@)\nSuperclass: %@\n\n",
            self.returnType,
            self.name,
            self.locations,
            [Symbol nameForSymbolType:self.type],
            self.minSDKVersion.major,
            self.minSDKVersion.minor,
            self.container.containerName,
            [Symbol nameForContainerType:self.container.type],
            self.superclassName];
}

#pragma mark - Identification/Export

- (NSString *)symbolKey {
    NSString *version = [NSString stringWithFormat:@"%d.%d", self.minSDKVersion.major, self.minSDKVersion.minor];
    NSString *name = [self.name lastPathComponent];
    NSString *containerName = [self.container.containerName lastPathComponent];
    NSMutableString *paramStr = [NSMutableString string];
    for (NSArray *oneParam in self.params) {
      [paramStr appendFormat:@",%@", oneParam[0]];
    }

    return [NSString stringWithFormat:@"%d<<##>>%@<<##>>%@<<##>>%@<<##>>%d<##>%@<##>%@<##>%@", self.type, name, version, containerName, self.container.type, self.superclassName, paramStr, self.returnType];
}

- (NSDictionary *)fullDictionary {
    NSString *type = [Symbol nameForSymbolType:self.type];
    NSString *minSDK = [NSString stringWithFormat:@"%d.%d", self.minSDKVersion.major, self.minSDKVersion.minor];
    NSDictionary *container = @{CONTAINER_NAME_KEY:self.container.containerName,
                                CONTAINER_TYPE_KEY:[Symbol nameForContainerType:self.container.type],
                                };
    NSDictionary *loc = [self createLocationDict];
    NSDictionary *info = @{TYPE_KEY:type,
                           NAME_KEY:self.name,
                           MIN_SDK_KEY:minSDK,
                           CONTAINER_KEY:container,
                           LOCATIONS_KEY:@[loc],
                           SUPERCLASS_KEY:self.superclassName,
                           RETURN_TYPE_KEY:self.returnType,
                           PARAMS_KEY:self.params,
                           UNCRITICAL_DECL_KEY:@(self.declarationIsUncritical)
                           };
    return info;
}

// return NO if symbol's location is already in the dictionary
- (BOOL)mergeIntoDictionary:(NSMutableDictionary *)existingSymbolEntries {
    NSArray *locations = [existingSymbolEntries objectForKey:LOCATIONS_KEY];

    NSDictionary *myLocation = [self createLocationDict];
    for (NSDictionary *location in locations) {
        if ([self locationDictsAreEqual:myLocation compareTo:location]) {
            return NO;
        }
    }
    // myLocation is not equal to any location that is already in the given dictionary

    NSMutableArray *locationsNew = [locations mutableCopy];
    [locationsNew addObject:myLocation];

    [existingSymbolEntries setObject:locationsNew forKey:LOCATIONS_KEY];

    return YES;
}

+ (NSString *)nameForContainerType:(AEWSymbolContainerType)type {
    NSArray *types = @[@"Class", @"Protocol", @"Category", @"File", @"None"];
    return types[type];
}

+ (AEWSymbolContainerType)typeForContainerTypeName:(NSString*)nameAsString {
    NSArray *types = @[@"Class", @"Protocol", @"Category", @"File", @"None"];
    return (AEWSymbolContainerType)[types indexOfObject:nameAsString];
}

+ (NSString*)nameForSymbolType:(AEWSymbolType)type {
    NSArray *types = @[@"Instance Method", @"Class Method", @"Property", @"Class", @"Subclass", @"C Function", @"Subscripting Method", @"Enum", @"Extern Definition", @"Unknown"];
    return types[type];
}

+ (AEWSymbolType)typeForSymbolName:(NSString*)name {
    NSArray *types = @[@"Instance Method", @"Class Method", @"Property", @"Class", @"Subclass", @"C Function", @"Subscripting Method", @"Enum", @"Extern Definition", @"Unknown"];
    return (AEWSymbolType)[types indexOfObject:name];
}

#pragma mark - Private

- (NSDictionary *)createLocationDict {
    return nil;

    /*return @{LOCATION_FILE_KEY:[NSString stringWithUTF8String:self.location.filename],
             LOCATION_LINE_KEY:@(self.location.lineNumberInFile)
             };*/
}

- (BOOL) locationDictsAreEqual:(NSDictionary*)dict1 compareTo:(NSDictionary*)dict2 {

    // for performance/hit rate reasons, compare lines first
    // it is quite likely that there are numerous occurrences in 1 file!
    int line1 = [[dict1 objectForKey:LOCATION_LINE_KEY] intValue];
    int line2 = [[dict2 objectForKey:LOCATION_LINE_KEY] intValue];

    if (line1 != line2) {
        return NO;
    }

    NSString *file1 = [dict1 objectForKey:LOCATION_FILE_KEY];
    NSString *file2 = [dict2 objectForKey:LOCATION_FILE_KEY];
    
    if (![file1 isEqualToString:file2]) {
        return NO;
    }
    
    return YES;
}

#pragma mark - Codegen

- (NSDictionary*) snippets:(NSString*)baseSnippetsDir {
    if (!_snippets) {
        NSString *subModulePath = [baseSnippetsDir stringByAppendingPathComponent:@"ExtendedCodeSnippets/index.plist"];
        _snippets = [NSDictionary dictionaryWithContentsOfFile:subModulePath];
        if (!_snippets) {
            _snippets = [NSDictionary dictionary];
        }
    }
    return _snippets;
}

- (NSString *)targetFileName {
    NSString *fileName;
    switch (self.type) {
        case AEWSymbolTypeCFunction:
            fileName = [self templateFileName];
            break;
        case AEWSymbolTypeInstanceMethod:
        case AEWSymbolTypeClassMethod:
        case AEWSymbolTypeProperty:
        case AEWSymbolTypeSubscriptingMethod:
            fileName = [self.container.containerName stringByAppendingString:@"+SHManaged"];
            break;
        case AEWSymbolTypeClass:
            fileName = [@"SHManaged" stringByAppendingString:self.name];
            break;
        case AEWSymbolTypeSubclass:
            fileName = [@"SHManaged" stringByAppendingString:self.superclassName];
            break;
        default:
            break;
    }
    return fileName;
}

- (NSString *)templateFileName {
    NSString *template;
    switch (self.type) {
        case AEWSymbolTypeCFunction:
            template = @"SHManagedCFunctions";
            break;
        case AEWSymbolTypeInstanceMethod:
        case AEWSymbolTypeClassMethod:
        case AEWSymbolTypeProperty:
        case AEWSymbolTypeSubscriptingMethod:
            template = @"LoadManipulator";
            break;
        case AEWSymbolTypeSubclass:
        case AEWSymbolTypeClass:
            template = @"ManagedClass";
            break;
        default:
            break;
    }
    return template;
}

- (NSString *)additionalTargetFileName {
    NSString *addition;
    switch (self.type) {
        case AEWSymbolTypeCFunction:
            addition = nil;
            break;
        case AEWSymbolTypeInstanceMethod:
        case AEWSymbolTypeProperty:
        case AEWSymbolTypeClassMethod:
        case AEWSymbolTypeSubscriptingMethod:
            // Category to load everything correctly
            // addition = [NSString stringWithFormat:@"%@+LoadManipulator", self.container.containerName];
            break;
        default:
            break;
    }
    return addition;
}

- (NSString *)additionalTemplateFileName {
    NSString *addition;
    switch (self.type) {
        case AEWSymbolTypeCFunction:
            break;
        case AEWSymbolTypeInstanceMethod:
        case AEWSymbolTypeClassMethod:
        case AEWSymbolTypeProperty:
        case AEWSymbolTypeSubscriptingMethod:
            // Category to load everything correctly
            // addition = @"LoadManipulator";
            // The implementation has changed:
            // All the code goes into the category
            break;
        default:
            break;
    }
    return addition;
}

- (NSString *)fallbackSelector {
    NSString *sel;
    switch (self.type) {
        case AEWSymbolTypeInstanceMethod:
        case AEWSymbolTypeClassMethod:
        case AEWSymbolTypeProperty:
        case AEWSymbolTypeSubscriptingMethod:
        {
            NSString *selName = self.name;
            NSString *firstChar = [selName substringToIndex:1];
            firstChar = [firstChar uppercaseString];
            selName = [selName stringByReplacingCharactersInRange:NSMakeRange(0, 1)
                                                       withString:firstChar];
            sel = [NSString stringWithFormat:@"SHManaged%@", selName];
            break;
        }
        default:
            break;
    }
    return sel;
}

- (NSString *)declaration {
    return [self declaration:YES];
}

- (NSString *)declaration:(BOOL)prependPrefix {
    NSString *decl;
    NSString *retType = (self.returnType)? self.returnType : @"void";
    static NSString *PREFIX = @"SHManaged";
    NSString *specifier = @"-";

    switch (self.type) {
        case AEWSymbolTypeCFunction:
            decl = [NSString stringWithFormat:@"%@ %@%@(%@)", retType, PREFIX, self.name, [self argumentsAsString:YES]];
            break;
        case AEWSymbolTypeClassMethod:
            specifier = @"+";
        case AEWSymbolTypeInstanceMethod:
        case AEWSymbolTypeProperty:
        case AEWSymbolTypeSubscriptingMethod:{
            // These are all instance methods
            NSString *selector = (prependPrefix)? self.fallbackSelector : self.name;
            NSString *sig = [Utility signatureFromSelector:selector
                                             andParameters:self.params];
            decl = [NSString stringWithFormat:@"%@ (%@) %@", specifier, retType, sig];
            break;
        }
        default:
            break;
    }
    return decl;
}

- (NSString *)definition:(NSString*)pathToSnippets {
    return [self definition:pathToSnippets usePrefix:YES];
}

- (NSString *)definition:(NSString*)pathToSnippets usePrefix:(BOOL)prefixFlag {
    NSString *def;
    NSString *decl = [self declaration:prefixFlag];

    // Maybe there's a specific snippet? (may return nil)
    NSString *snippet;
    NSString *specificSnippetPath = [[self snippets:pathToSnippets] objectForKey:self.name];
    if (specificSnippetPath) {
        specificSnippetPath = [pathToSnippets stringByAppendingFormat:@"/ExtendedCodeSnippets/%@", specificSnippetPath];
        snippet = [NSString stringWithContentsOfFile:specificSnippetPath
                                            encoding:NSUTF8StringEncoding
                                               error:nil];
    }
    
    NSError *error;
    NSString *snippetName;
    static NSString *DEFAULT_RETURN = @"###DEFAULT_RETURN###";
    switch (self.type) {
        case AEWSymbolTypeCFunction: {
            
            if (!snippet) {
                // Default snippets
                snippetName = ([self isVoid])? @"VoidFunctionCheck.h" : @"FunctionCheck.h";
                NSString *filePath = [pathToSnippets stringByAppendingPathComponent:snippetName];
                snippet = [NSString stringWithContentsOfFile:filePath
                                                    encoding:NSUTF8StringEncoding
                                                       error:&error];
            }

            static NSString *SYMBOL_NAME = @"###SYMBOL_NAME###";
            static NSString *SYMBOL_ARGS = @"###SYMBOL_ARGS###";
            snippet = [snippet stringByReplacingOccurrencesOfString:SYMBOL_NAME withString:self.name];
            snippet = [snippet stringByReplacingOccurrencesOfString:SYMBOL_ARGS withString:[self argumentsAsString:NO]];
            if (![self isVoid]) {
                snippet = [snippet stringByReplacingOccurrencesOfString:DEFAULT_RETURN
                                                             withString:[self defaultReturnValue]];
            }
            def = [NSString stringWithFormat:@"%@ {\n%@\n}", decl, snippet];
            break;
        }
        case AEWSymbolTypeClassMethod:
            if (!prefixFlag && !snippet) {
                if (!self.declarationIsUncritical) {
                    snippetName = ([self isVoid])? @"CriticalVoidClassMethod.h":@"CriticalClassMethod.h";
                } else {
                    snippetName = ([self isVoid])? @"UncriticalVoidClassMethod.h":@"UncriticalClassMethod.h";
                }
            }
        case AEWSymbolTypeInstanceMethod:
        case AEWSymbolTypeProperty:
        case AEWSymbolTypeSubscriptingMethod:
            if (!prefixFlag) {
                // We can help here: Various snippets available
                if (!self.declarationIsUncritical && !snippetName) {
                    snippetName = ([self isVoid])? @"CriticalVoidMethod.h":@"CriticalMethod.h";
                } else if (!snippetName) {
                    snippetName = ([self isVoid])? @"UncriticalVoidMethod.h":@"UncriticalMethod.h";
                }
                if (!snippet) {
                    snippetName = [pathToSnippets stringByAppendingPathComponent:snippetName];
                    snippet = [NSString stringWithContentsOfFile:snippetName
                                                        encoding:NSUTF8StringEncoding
                                                           error:nil];
                }

                // Customise snippet
                static NSString *SEL_WITH_ARGS = @"###SEL_AND_ARGS###";
                static NSString *CAST = @"###CAST###";
                static NSString *ORIG_CLASS = @"###ORIGINAL_CLASS###";
                NSString *selAndArgs = [Utility selector:self.name withArgs:self.params];
                snippet = [snippet stringByReplacingOccurrencesOfString:SEL_WITH_ARGS
                                                             withString:selAndArgs];
                snippet = [snippet stringByReplacingOccurrencesOfString:ORIG_CLASS
                                                             withString:self.container.containerName];
                NSString *cast = @"";
                if ([self.name hasPrefix:@"init"]
                    || [self.name hasPrefix:@"new"]) {
                    cast = [NSString stringWithFormat:@"(SHManaged%@*)", self.container.containerName];
                }
                snippet = [snippet stringByReplacingOccurrencesOfString:CAST
                                                             withString:cast];

            } else {
                // There's no way to foresee the implementation
                // Just leave a warning
                if (!snippet) {
                    snippet = @"    // Note: This implementation is only used when the native framework\n    // implementation is missing\n    #warning Missing fallback implementation";
                }

                if (![self isVoid]) {
                    snippet = [snippet stringByAppendingString:@"\n    return ###DEFAULT_RETURN###;"];
                }
            }
            if (![self isVoid]) {
                snippet = [snippet stringByReplacingOccurrencesOfString:DEFAULT_RETURN
                                                             withString:[self defaultReturnValue]];
            }
            def = [NSString stringWithFormat:@"%@ {\n%@\n}", decl, snippet];
            break;
        default:
            break;
    }
    if (error) {
        NSLog(@"Error loading snippet for %@.", decl);
    }
    return def;
}

- (BOOL) isVoid {
    if (!self.returnType ||
        [self.returnType isEqualToString:@""] ||
        [self.returnType isEqualToString:@"void"] ||
        [self.returnType isEqualToString:@"IBAction"] ||
        [self.returnType isEqualToString:@"NULL TYPE"]) {
        return YES;
    }
    return NO;
}

- (NSString*) defaultReturnValue {
    NSString *retVal;
    if ([self.returnType rangeOfString:@"*"].location != NSNotFound) {
        // Object pointer
        retVal = @"nil";
    } else if ([self.returnType rangeOfString:@"bool" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        // Some kind of boolean type
        retVal = @"NO";
    } else {
        // id?
        if ([self.returnType isEqualToString:@"id"] ||
            [self.returnType isEqualToString:@"instancetype"]) {
            // init method? --> return [super init]
            if ([self.name isEqualToString:@"init"] &&
                self.type == AEWSymbolTypeInstanceMethod) {
                retVal = @"[super init]";
            } else {
                retVal = @"nil";
            }
        } else {
            // Other primitive type
            retVal = @"0";
        }
    }
    return retVal;
}

- (NSString *) argumentsAsString:(BOOL)includeTypes {
    NSString *args = @"";
    switch (self.type) {
        case AEWSymbolTypeCFunction: {
            args = [NSMutableString string];
            NSUInteger size = self.params.count;
            [self.params enumerateObjectsUsingBlock:^(NSArray *obj, NSUInteger idx, BOOL *stop) {
                NSString *oneArg;
                if (includeTypes) {
                    oneArg = [NSString stringWithFormat:@"%@ %@", obj[0], obj[1]];
                } else {
                    oneArg = (NSString*)obj[1];
                }
                if (idx == (size-1)) {
                    // last one, no more comma
                    [(NSMutableString*)args appendString:oneArg];
                } else {
                    [(NSMutableString*)args appendFormat:@"%@, ", oneArg];
                }
            }];
            break;
        }
        default:
            break;
    }
    return args;
}

- (NSString*)searchString:(BOOL)prependPrefix {
    return [self declaration:prependPrefix];
}

- (NSString *)headerDocumentation:(NSString *)pathToSnippets {
    static NSString *FILE_NAME = @"Doc.h";
    NSString *pathToFile = [pathToSnippets stringByAppendingPathComponent:FILE_NAME];
    NSMutableString *content = [NSMutableString stringWithContentsOfFile:pathToFile
                                                  encoding:NSUTF8StringEncoding
                                                     error:nil];
    // Replace placeholders

    // Insert type name
    static NSString *TYPE = @"###SYMBOL_TYPE###";
    static NSString *DECL_LOC = @"###DECLARATION_LOC###";
    static NSString *AV = @"###AVAILABILITY###";
    static NSString *REFS = @"###REFERENCES###";

    NSString *decl = [NSString stringWithFormat:@"%@ '%@'",
                      [Symbol nameForContainerType:self.container.type],
                      [self.container.containerName lastPathComponent]];
    NSString *version = [NSString stringWithFormat:@"iOS %d.%d",
                         self.minSDKVersion.major,
                         self.minSDKVersion.minor];
    if (self.declarationIsUncritical) {
        version = [version stringByAppendingFormat:@" (It was declared earlier, but may have been\n *  overwritten in a later version)"];
    }

    [content replaceOccurrencesOfString:TYPE
                             withString:[Symbol nameForSymbolType:self.type]
                                options:NSLiteralSearch
                                  range:NSMakeRange(0, content.length)];
    [content replaceOccurrencesOfString:DECL_LOC
                             withString:decl
                                options:NSLiteralSearch
                                  range:NSMakeRange(0, content.length)];
    [content replaceOccurrencesOfString:AV
                             withString:version
                                options:NSLiteralSearch
                                  range:NSMakeRange(0, content.length)];
    [content replaceOccurrencesOfString:REFS
                             withString:[self headerDocumentationReferences]
                                options:NSLiteralSearch
                                  range:NSMakeRange(0, content.length)];

    return content;
}

- (NSString *)headerDocumentationReferences {
    __block NSMutableString *refs = [NSMutableString string];
    [self.locations enumerateObjectsUsingBlock:^(SourceLocation *loc, NSUInteger idx, BOOL *stop) {
        [refs appendFormat:@" *  [%@:%@]\n", [loc.fileName lastPathComponent], loc.lineNumber];
    }];
    return refs;
}

@end
