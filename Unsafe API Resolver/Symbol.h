//
//  Symbol.h
//  Unsafe API Resolver
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import <Foundation/Foundation.h>

#import "SymbolContainer.h"
#import "SourceLocation.h"

#pragma mark - Definitions

/// Represents the type of a symbol
typedef enum AEWSymbolType {

    AEWSymbolTypeInstanceMethod,
    AEWSymbolTypeClassMethod,
    AEWSymbolTypeProperty,
    AEWSymbolTypeClass,
    AEWSymbolTypeSubclass,
    AEWSymbolTypeCFunction,
    AEWSymbolTypeSubscriptingMethod,
    AEWSymbolTypeEnum,
    AEWSymbolTypeExternDefinition,
    AEWSymbolTypeUnknown

} AEWSymbolType;

/// Represents the iOS version
typedef struct {
    
    int major;
    int minor;
    
} AEWiOSVersion;

#pragma mark - Keys for the dictionary representation

// Input: Symbols serialised in a PLIST
#define TYPE_KEY @"type"
#define NAME_KEY @"name"
#define MIN_SDK_KEY @"minSDK"
#define CONTAINER_KEY @"container"
    #define CONTAINER_NAME_KEY @"c_name"
    #define CONTAINER_TYPE_KEY @"c_type"
#define LOCATIONS_KEY @"locations"
    #define LOCATION_FILE_KEY @"l_file"
    #define LOCATION_LINE_KEY @"l_line"
#define SUPERCLASS_KEY @"superclass"
#define RETURN_TYPE_KEY @"return_type"
#define PARAMS_KEY @"params"
#define UNCRITICAL_DECL_KEY @"uncritical_decl"

#pragma mark - Public Interface

/**
 *  This class represents one source code symbol, e.g., a
 *  method, class, or function.
 */
@interface Symbol : NSObject

#pragma mark - Factory

/**
 *  Instantiate a new symbol object with the information
 *  from the given dictionary.
 */
+ (Symbol*) symbolFromDictionary:(NSDictionary*)symbolInfo;

#pragma mark - Typed properties for one symbol

/**
 *  The type of the symbol, e.g., a method or a function.
 */
@property (assign) AEWSymbolType type;

/**
 *  The container in which the symbol was originally
 *  declared (not used!), e.g., a class interface or a protocol.
 */
@property (strong) SymbolContainer *container;

/**
 *  The location at which a symbol has been found. (i.e.,
 *  where it is referenced)
 */
@property (strong) NSArray *locations;

/**
 *  The SDK version which first introduced the symbol.
 */
@property (assign) AEWiOSVersion minSDKVersion;

/**
 *  The symbol's full name.
 *
 *  For methods, this equals the selector.
 */
@property (copy) NSString *name;

/**
 *  If the symbol's type is 'Subclass', the name of the
 *  superclass should be provided.
 */
@property (copy) NSString *superclassName;

/**
 *  The return type of a symbol, as a string value.
 */
@property (strong) NSString* returnType;

/**
 *  The parameters of C functions and methods.
 *
 *  Each entry is another array. The type is stored
 *  as index 0, the parameter name at index 1. Both
 *  are of type NSString.
 */
@property (strong) NSArray *params;

/**
 *  Normally set to NO. Example where this could
 *  be set to YES:
 *
 *  -'description' was called on critical receiver
 *  -'description' is therefore considered critical
 *  -however, 'description' is declared in NSObject,
 *  so sending this message to any proxy object
 *  would not crash, but may not result in the
 *  desired behaviour.
 */
@property (assign) BOOL declarationIsUncritical;

#pragma mark - Export and identification properties

/**
 *  Key containing type, name, minSDK and container fields.
 *
 *  Should uniquely identify one type of symbol, but not
 *  the source location where it was referenced. It may
 *  include it's declaration location via the container.
 */
- (NSString*) symbolKey;

/**
 *  Returns a dictionary with all the symbol's information,
 *  having exactly one source code location
 */
- (NSDictionary*) fullDictionary;

/**
 *  Descriptive name for the type, e.g., "Method"
 */
+ (NSString*) nameForSymbolType:(AEWSymbolType)type;

/**
 *  Descriptive name for the container's type, e.g., "Category"
 */
+ (NSString*) nameForContainerType:(AEWSymbolContainerType)type;

/**
 *  Print all information about this symbol
 */
- (NSString *)debugDescription;

/**
 *  A set of symbols which belong to the parent symbol.
 *
 *  Example: A class can have a set of methods.
 *
 *  The dictionary has the symbols' name as key, and the
 *  symbols itself as value.
 */
@property (strong) NSDictionary *subSymbols;

#pragma mark - Codegen

/**
 *  Each symbol must know which file it should end up in.
 *
 *  All functions are currently put into one file, whereas
 *  classes and methods use the normal 1 class/1 file convention.
 */
- (NSString *) targetFileName;

/**
 *  If a new file needs to be created, a template file is used
 *  and adopted. Each type of symbol must know its template's
 *  file name.
 */
- (NSString *) templateFileName;

/**
 *  Some symbols may need more than one header/implementation
 *  file pair to be resolved correctly, e.g., an extra
 *  category.
 */
- (NSString *)additionalTargetFileName;

/**
 *  Some symbols may need more than one header/implementation
 *  file pair to be resolved correctly, e.g., an extra
 *  category.
 */
- (NSString *)additionalTemplateFileName;

/**
 *  The selector which is used in case a symbol cannot be
 *  found should follow a fixed naming convention.
 */
- (NSString *)fallbackSelector;

/**
 *  The declaration of the symbol in source code, without
 *  a trailing ';' to make it reusable for the definition.
 * 
 *  Prepends the 'SHManaged' prefix.
 */
- (NSString *) declaration;

/**
 *  The declaration of the symbol in source code, without
 *  a trailing ';' to make it reusable for the definition.
 *
 *  Choose whether or not to prepend 'SHManaged'.
 */
- (NSString *) declaration:(BOOL)prependPrefix;

/**
 *  The definition incl. a default implementation. For the latter,
 *  it may be necessary to load code snippets from files.
 *
 *  @param pathToSnippets: The path to the directory with the snippets.
 *
 *  Prepends the 'SHManaged' prefix.
 */
- (NSString *) definition:(NSString*)pathToSnippets;

/**
 *  The definition incl. a default implementation. For the latter,
 *  it may be necessary to load code snippets from files.
 *
 *  @param pathToSnippets: The path to the directory with the snippets.
 *
 *  Choose whether or not to prepend 'SHManaged'.
 */
- (NSString *)definition:(NSString*)pathToSnippets usePrefix:(BOOL)prefixFlag;

/**
 *  Before new files or declarations are added, the tool will check
 *  whether the symbol already exists. It will use this string and
 *  look for it in all appropriate places. It must be unique for each
 *  symbol.
 */
- (NSString *) searchString:(BOOL)prependPrefix;

/**
 *  Each generated declaration should have a header to document it.
 */
- (NSString *) headerDocumentation:(NSString*)pathToSnippets;

/**
 *  The 'References' part of the header documentation
 */
- (NSString *) headerDocumentationReferences;

@end
