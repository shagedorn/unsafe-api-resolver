# Unsafe API Resolver Tools: Copyright & warranty notice #

## LEGAL ##

### COPYRIGHT & WARRANTY NOTICE (Unsafe API Resolver) ###

Unsafe API Resolver was written by Sebastian Hagedorn as part of a diploma thesis at
TU Dresden. It was published under the following license (FreeBSD):

> Copyright (c) 2013 Sebastian Hagedorn  
> All rights reserved.
> 
> Redistribution and use in source and binary forms, with or without modification,
> are permitted provided that the following conditions are met:
> 
> 1. Redistributions of source code must retain the above copyright notice, this
> list of conditions and the following disclaimer.  
> 2. Redistributions in binary form must reproduce the above copyright notice, this
> list of conditions and the following disclaimer in the documentation and/or other
> materials provided with the distribution.  
> 
> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
> ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
> IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
> INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
> BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
> DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
> LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
> OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
> THE POSSIBILITY OF SUCH DAMAGE.

### COPYRIGHT & WARRANTY NOTICE (Third Party Software) ###

This license applies to all files in this repository, except for the following:

+ `/Resources/Tools/bin/clang-detect`: Fork of the Clang compiler (Binary)  
+ `/Resources/Tools/bin/clang-resolve`: Fork of the Clang compiler (Binary)  
+ `/Resources/Tools/mod-pbxproj`

#### COPYRIGHT & WARRANTY NOTICE (Clang/LLVM) ####

The Clang compiler was published under the following license (University of Illinois 
Open Source License):

> University of Illinois/NCSA  
> Open Source License

> Copyright (c) 2003-2013 University of Illinois at Urbana-Champaign.  
> All rights reserved.

> Developed by:
>
>    LLVM Team
>
>    University of Illinois at Urbana-Champaign
>
>    [http://llvm.org](http://llvm.org)
>
> Permission is hereby granted, free of charge, to any person obtaining a copy of
> this software and associated documentation files (the "Software"), to deal with
> the Software without restriction, including without limitation the rights to
> use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
> of the Software, and to permit persons to whom the Software is furnished to do
> so, subject to the following conditions:
>
>   * Redistributions of source code must retain the above copyright notice,
>     this list of conditions and the following disclaimers.
>
>   * Redistributions in binary form must reproduce the above copyright notice,
>      this list of conditions and the following disclaimers in the
>      documentation and/or other materials provided with the distribution.
>
>   * Neither the names of the LLVM Team, University of Illinois at
>      Urbana-Champaign, nor the names of its contributors may be used to
>      endorse or promote products derived from this Software without specific
>      prior written permission.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
> FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
> CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
> LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
> OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE
> SOFTWARE. 
 
See [LICENSE.txt](http://llvm.org/svn/llvm-project/llvm/trunk/LICENSE.TXT) for the full
license including licenses of third party software distributed with Clang/LLVM.

#### COPYRIGHT & WARRANTY NOTICE (mod-pbxproj) ####

mod-pbxproj was published under the following license (Apache License, Version 2):

>  Copyright 2012 Calvin Rien
>
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
>
>  http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.

[Repository](https://github.com/kronenthaler/mod-pbxproj)
