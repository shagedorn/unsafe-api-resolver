//
//  main.m
//  XcodeProjectCheck
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        /**
         *  Checks:     *All critical frameworks weakly linked?
         *              *TODO: Critical FWs in libraries?
         *              *TODO: Export critical frameworks' symbol prefixes
         *              *XIB Files: Depl. Target = Project Depl. Target?
         *                          Check for AutoLayout specifically
         *              *Storyboard: Not allowed prior to iOS 5
         *                           AutoLayout not allowed prior to iOS 6
         *              *CoreData: Compare Depl. Target
         */
        NSDictionary *environment = [[NSProcessInfo processInfo] environment];
        static NSString *PROJ_KEY = @"PROJECT_FILE_PATH";
        NSString *projectName = [environment objectForKey:PROJ_KEY];

        static NSString *PROJECT_FILE_NAME = @"project.pbxproj";
        NSString *projectInfosFileName = [projectName stringByAppendingPathComponent:PROJECT_FILE_NAME];
        NSDictionary *projectInfos = [NSDictionary dictionaryWithContentsOfFile:projectInfosFileName];
        
#pragma mark - Frameworks
        
        // Get dictionary with availability information
        NSString *resourcesFolderPath = [[projectName stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Resources"];
        NSString *frameworksPlistPath = [resourcesFolderPath stringByAppendingPathComponent:@"FrameworkVersionInfo/Frameworks.plist"];
        NSDictionary *frameworksPlist = [NSDictionary dictionaryWithContentsOfFile:frameworksPlistPath];

        // Get the deployment target of the current target/app
        float deplTarget = [[environment objectForKey:@"IPHONEOS_DEPLOYMENT_TARGET"] floatValue];
        // Get the name of the active target
        NSString *targetName = [environment objectForKey:@"TARGETNAME"];

        // Get the project ref
        projectInfos = [projectInfos objectForKey:@"objects"];
        NSString *isaString;
        NSDictionary *theProject;
        for (NSDictionary *oneObject in projectInfos.allValues) {
            isaString = [oneObject objectForKey:@"isa"];
            if ([isaString isEqualToString:@"PBXProject"]) {
                theProject = oneObject;
                break;
            }
        }
        // Get the targets - pick the right one
        NSArray *targetRefs = [theProject objectForKey:@"targets"];
        NSDictionary *theTarget;
        for (NSString *targetRef in targetRefs) {
            NSDictionary *targetInfos = [projectInfos objectForKey:targetRef];
            if ([[targetInfos objectForKey:@"name"] isEqualToString:targetName]) {
                // We found it - validate
                if ([[targetInfos objectForKey:@"isa"] isEqualToString:@"PBXNativeTarget"]) {
                    theTarget = targetInfos;
                    break;
                }
            }
        }
        // Get the correct build phase
        NSArray *buildPhases = [theTarget objectForKey:@"buildPhases"];
        NSArray *fileRefs;
        NSArray *resourceFileRefs;
        for (NSString *buildPhaseRef in buildPhases) {
            NSDictionary *phase = [projectInfos objectForKey:buildPhaseRef];
            if ([[phase objectForKey:@"isa"] isEqualToString:@"PBXFrameworksBuildPhase"]) {
                fileRefs = [phase objectForKey:@"files"];
            } else if ([[phase objectForKey:@"isa"] isEqualToString:@"PBXResourcesBuildPhase"]) {
                resourceFileRefs = [phase objectForKey:@"files"];
            }
        }
        // Go through every file (=framework): Weak-linked, if critical?
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setMaximumFractionDigits:1];
        [formatter setMinimumFractionDigits:1];

        NSString *deploymentTarget = [formatter stringFromNumber:@(deplTarget)];
        for (NSString *fileRef in fileRefs) {
            NSDictionary *buildFile = [projectInfos objectForKey:fileRef];

            // Already weak-linked? --> No need for further checks
            NSDictionary *settings = [buildFile objectForKey:@"settings"];
            NSArray *atts = [settings objectForKey:@"ATTRIBUTES"];
            BOOL skip = NO;
            for (NSString *attr in atts) {
                if ([attr isEqualToString:@"Weak"]) {
                    // Skip to next symbol
                    skip = YES;
                    break;
                }
            }
            if (skip) {
                continue;
            }

            // Not weak-linked --> Check if critical
            NSString *frameworkFileRef = [buildFile objectForKey:@"fileRef"];
            NSDictionary *frameworkFile = [projectInfos objectForKey:frameworkFileRef];
            NSString *frameworkName = [frameworkFile objectForKey:@"name"];
            
            // 1st Check: .framework extension?
            NSString *ext = [frameworkName pathExtension];
            if ([ext isEqualToString:@"framework"]) {
                NSString *bareName = [frameworkName stringByDeletingPathExtension];
                // Look up in availability dict
                NSDictionary *frameworkInfo = [frameworksPlist objectForKey:bareName];
                NSNumber *minSDK = [frameworkInfo objectForKey:@"MinSDK"];

                if (minSDK.floatValue > deplTarget) {
                    // This one is critical - weak link!
                    printf("%s:0: warning: %s was introduced later than the deployment target (%s > %s). Set this framework to 'optional' to avoid crashes on legacy systems.\n",
                           [projectName UTF8String],
                           [frameworkName UTF8String],
                           [[formatter stringFromNumber:minSDK] UTF8String],
                           [deploymentTarget UTF8String]);
                }
            }
        }
        return 0;
        
        // Rest is unfinished/experimental

#pragma mark - XIBs

        // Get all .XIBs from Copy Bundle Resources Phase
        for (NSString *resourceFileRef in resourceFileRefs) {
            NSDictionary *resFile = [projectInfos objectForKey:resourceFileRef];
            NSString *xibFileRef = [resFile objectForKey:@"fileRef"];

            // This is just a container for (possibly) many localised files
            NSDictionary *xibFileContainer = [projectInfos objectForKey:xibFileRef];
            if ([[xibFileContainer objectForKey:@"isa"] isEqualToString:@"PBXVariantGroup"]) {
                NSString *symbolicName = [xibFileContainer objectForKey:@"name"];
                if (![[symbolicName pathExtension] isEqualToString:@"xib"]) {
                    // Other resource file
                    continue;
                }
                // It's a XIB: Check, if all children (=localisations) are safe
                NSArray *children = [xibFileContainer objectForKey:@"children"];
                
                NSMutableArray *unsafeChildren = [NSMutableArray arrayWithCapacity:children.count];
                NSMutableArray *autoLayoutChildren = [NSMutableArray arrayWithCapacity:children.count];
                
                NSString *xibDeplTarget;
                for (NSString *childRef in children) {
                    NSDictionary *child = [projectInfos objectForKey:childRef];

                    // Make the actual check!
                    BOOL isSafe = YES;
                    BOOL usesAutoLayout = NO;
                    xibDeplTarget = @"6.0"; // use the lowest
                    // TODO: check and set depl target
                    isSafe = NO;
                    usesAutoLayout = YES;

                    if (!isSafe) {
                        // Only safe the name of the localisation
                        [unsafeChildren addObject:[child objectForKey:@"name"]];
                    }
                    if (usesAutoLayout) {
                        [autoLayoutChildren addObject:[child objectForKey:@"name"]];
                    }
                }

                // Summary for 1 XIB
                if (unsafeChildren.count > 0) {
                    // At least one problem
                    // NSString *absoluteFilePath = [];

                    NSString *addition = @"";
                    if (unsafeChildren.count != children.count) {
                        addition = [NSString stringWithFormat:@" These localizations are affected: %@", [unsafeChildren componentsJoinedByString:@", "]];
                    }
                    printf("%s:0: warning: the XIB file's deployment target is higher than the current target's deployment target (%s > %s). Set it to %s to enable correct API checks within the XIB file.%s",
                           [symbolicName UTF8String],
                           [xibDeplTarget UTF8String],
                           [deploymentTarget UTF8String],
                           [deploymentTarget UTF8String],
                           [addition UTF8String]);
                }
                // TODO: report autolayout
            }
        }
        
        
    }
    return 0;
}

