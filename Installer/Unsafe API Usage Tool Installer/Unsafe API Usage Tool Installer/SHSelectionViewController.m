//
//  SHSelectionViewController.m
//  Unsafe API Usage Tool Installer
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import "SHSelectionViewController.h"

typedef enum SHTask {
    SHTaskCopyClang,
    SHTaskCC,
    SHTaskObjC,
    SHTaskCopyFolder,
    SHTaskAddPhases,
    SHTaskAddSubproj,
    SHTaskImportHeader,
    SHTaskSyncDeplTargets,
    SHTaskSearchPaths,
    SHTaskExportFlag
} SHTask;

@interface SHSelectionViewController ()

@property (weak) IBOutlet NSButton *applyResolutionButton;
@property (weak) IBOutlet NSButton *applyDetectionButton;
@property (weak) IBOutlet NSProgressIndicator *progressIndicator;

@end

@implementation SHSelectionViewController

#pragma mark - Actions

- (IBAction)applyDetection:(id)sender {
    __block NSArray *taskList = @[@(SHTaskCopyClang),
                          @(SHTaskCC)];
    
    [[NSOperationQueue new] addOperationWithBlock:^{
        [self executeTasks:taskList resolutionPhase:NO];
    }];
}

- (IBAction)applyResolution:(id)sender {
    __block NSArray *taskList = @[@(SHTaskCopyClang),
                          @(SHTaskCC),
                          @(SHTaskObjC),
                          @(SHTaskCopyFolder),
                          @(SHTaskAddPhases),
                          @(SHTaskAddSubproj),
                          @(SHTaskImportHeader),
                          @(SHTaskSyncDeplTargets),
                          @(SHTaskSearchPaths),
                          @(SHTaskExportFlag)];
    
    [[NSOperationQueue new] addOperationWithBlock:^{
        [self executeTasks:taskList resolutionPhase:YES];
    }];
}

// This should be executed in background - UI changes must therefore
// explicitly be made on the main queue
- (void) executeTasks:(NSArray*)taskList resolutionPhase:(BOOL)resolve{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        self.applyDetectionButton.enabled = NO;
        self.applyResolutionButton.enabled = NO;
        [self.progressIndicator startAnimation:nil];
    }];

    // Load project file and target once, not for every task separately
    NSString *fullProjectFilePath = [self.projectFilePath
                                     stringByAppendingPathComponent:@"project.pbxproj"];
    NSMutableDictionary *project = [NSDictionary dictionaryWithContentsOfFile:fullProjectFilePath];

    // All elements are now mutable
    project = CFBridgingRelease(CFPropertyListCreateDeepCopy(NULL,
                                                             (__bridge CFPropertyListRef)(project),
                                                             kCFPropertyListMutableContainersAndLeaves));

    // Some standard objects that will be used more than once
    NSMutableDictionary *target = [self findAppTarget:project idOnly:NO];
    NSString *buildConfigsId = target[@"buildConfigurationList"];
    NSMutableDictionary *buildConfigs = [self findObjectById:buildConfigsId inProject:project];
    NSMutableArray *allTargetConfigsIds = buildConfigs[@"buildConfigurations"];
    NSMutableDictionary *debugConfig;
    for (NSString *configId in allTargetConfigsIds) {
        NSMutableDictionary *config = [self findObjectById:configId inProject:project];
        if ([config[@"name"] isEqualToString:@"Debug"]) {
            // The debug configuration
            debugConfig = config;
            break;
        }
    }
    if (!debugConfig) {
        NSLog(@"Warning: Debug configuration not found.");
    }

    for (NSNumber *taskId in taskList) {
        BOOL retVal = NO;
        switch (taskId.integerValue) {
            case SHTaskCopyClang:
                retVal = [self copyClang:resolve];
                break;
            case SHTaskCC:
                retVal = [self setCC:debugConfig resolution:resolve];
                break;
            case SHTaskObjC:
                retVal = [self setObjC:allTargetConfigsIds in:project];
                break;
            case SHTaskCopyFolder:
                retVal = [self copyResourcesFolder];
                break;
            case SHTaskAddPhases:
                retVal = [self addBuildPhases:target of:project];
                break;
            case SHTaskAddSubproj:
                retVal = [self addSubproject:project];
                break;
            case SHTaskImportHeader:
                retVal = [self importHeader:project];
                break;
            case SHTaskSyncDeplTargets:
                retVal = [self syncDeploymentTargets:project];
                break;
            case SHTaskSearchPaths:
                retVal = [self addSearchPaths:project toConfigs:allTargetConfigsIds];
                break;
            case SHTaskExportFlag:
                retVal = [self setExportFlag:debugConfig];
                break;
            default:
                break;
        }
        if (retVal) {
            // Show green checkmark
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                NSView *checkmark = [self viewForTask:(SHTask)taskId.integerValue
                                           resolution:resolve];
                checkmark.hidden = NO;
            }];
        }
    }


    // Write back PLIST
    [project writeToFile:fullProjectFilePath atomically:YES];

    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [self.progressIndicator stopAnimation:nil];
    }];
}

#pragma mark - Helper

- (NSString*) deploymentTargetFromConfigurationList:(NSString*)configListId
                                                 in:(NSDictionary*)project {
    NSDictionary *configList = [self findObjectById:configListId
                                          inProject:project];
    NSArray *configIds = configList[@"buildConfigurations"];
    NSInteger major = -1;
    NSInteger minor = -1;
    float targetAsFloat = 0.0;
    for (NSString *configId in configIds) {
        NSDictionary *configuration = [self findObjectById:configId inProject:project];
        NSDictionary *settings = configuration[@"buildSettings"];
        NSString *dTarget = settings[@"IPHONEOS_DEPLOYMENT_TARGET"];
        if (dTarget && dTarget.length > 0) {
            // Compare: remember the lowest
            NSArray *components = [dTarget componentsSeparatedByString:@"."];
            NSInteger thisMajor = [components[0] integerValue];
            NSInteger thisMinor = [components[1] integerValue];
            float thisTargetAsFloat = [dTarget floatValue];
            if (major < 0) {
                major = thisMajor;
                minor = thisMinor;
                targetAsFloat = major + (minor/10);
            } else {
                // Actually compare
                if (thisTargetAsFloat <= targetAsFloat) {
                    major = thisMajor;
                    minor = thisMinor;
                    targetAsFloat = major + (minor/10);
                }
            }
        }
    }
    if (major < 0) {
        return nil;
    }
    return [NSString stringWithFormat:@"%ld.%ld", (long)major, (long)minor];
}

- (NSMutableDictionary*) findAppTarget:(NSDictionary*)projectFile
                                idOnly:(BOOL)returnId {
    return [self findTargetOfType:@"com.apple.product-type.application"
                               in:projectFile
                           idOnly:returnId];
}

- (NSMutableDictionary*) findLibraryTarget:(NSDictionary*)projectFile
                                    idOnly:(BOOL)returnId {
    return [self findTargetOfType:@"com.apple.product-type.library.static"
                               in:projectFile
                           idOnly:returnId];
}

- (NSMutableDictionary*) findTargetOfType:(NSString*)targetTypeName
                                       in:(NSDictionary*)projectFile
                                   idOnly:(BOOL)returnId {
    NSString *rootObjectId = projectFile[@"rootObject"];
    NSMutableDictionary *xcodeProject = [self findObjectById:rootObjectId inProject:projectFile];
    NSMutableArray *targets = xcodeProject[@"targets"];
    for (NSString *targetId in targets) {
        NSMutableDictionary *target = [self findObjectById:targetId
                                                 inProject:projectFile];
        // The actual app?
        NSString *type = target[@"productType"];
        if ([type isEqualToString:targetTypeName]) {
            if (returnId) {
                return [NSMutableDictionary dictionaryWithObject:targetId
                                                          forKey:@"targetId"];
            } else {
                return target;
            }
        }
    }
    return nil;
}

- (NSView*) viewForTask:(SHTask)taskId resolution:(BOOL)resolutionPhase {
    int viewTag = (resolutionPhase)? taskId+2000 : taskId+1000;
    return [self.view viewWithTag:viewTag];
}

- (NSMutableDictionary*) findObjectById:(NSString*)identifier
                       inProject:(NSDictionary*)project {
    NSMutableDictionary *allObjects = project[@"objects"];
    return allObjects[identifier];
}

- (BOOL) copyFileFrom:(NSString*)sourcePath
                   to:(NSString*)destPath
          isDirectory:(BOOL)directory
            overwrite:(BOOL)overwriteExistingFile
                error:(NSError**)error {
    
    NSFileManager *fm = [NSFileManager defaultManager];

    // Does destination folder exist?
    NSString *destFolder = [destPath stringByDeletingLastPathComponent];
    
    if (![fm fileExistsAtPath:destFolder]) {
        [fm createDirectoryAtPath:destFolder
      withIntermediateDirectories:YES
                       attributes:nil
                            error:error];
        if (*error) {
            NSLog(@"Could not create directory '%@': %@", destFolder, *error);
            return NO;
        }
    }

    // Overwrite check
    BOOL fileAlreadyInPlace = [fm fileExistsAtPath:destPath];
    if (!overwriteExistingFile && fileAlreadyInPlace) {
        // Don't overwrite
        return YES;
    } else {
        if (overwriteExistingFile && fileAlreadyInPlace) {
            // Overwrite/remove old
            [fm removeItemAtPath:destPath error:error];
        }
        // Create new
        [fm copyItemAtPath:sourcePath
                    toPath:destPath
                     error:error];
        if (*error) {
            NSLog(@"Could not copy '%@' to '%@': %@",
                  sourcePath.lastPathComponent,
                  destPath,
                  *error);
        }
    }
    return YES;
}

- (NSString*) addObjectToProject:(NSDictionary*)object project:(NSMutableDictionary*)project {
    // We need to create a new, unique id
    NSMutableDictionary *objects = project[@"objects"];
    NSString *anyKey = [objects.allKeys lastObject];
    
    BOOL keyExists = YES;
    int count = 0; // Don't try forever...
    NSString *newKey;
    while (keyExists) {
        // Try random replacements
        NSString *letters = @"ABCDEF0123456789";
        NSInteger r = arc4random() % anyKey.length;
        NSInteger i = arc4random() % letters.length;
        newKey = [anyKey stringByReplacingCharactersInRange:NSMakeRange(r, 1)
                                                           withString:[letters substringWithRange:NSMakeRange(i, 1)]];
        id test = [self findObjectById:newKey inProject:project];
        if (!test) {
            keyExists = NO;
        }
        
        count++;
        if (count > 10000) {
            NSLog(@"Error: Could not generate a new Xcode project object ID.");
            return nil;
        }
    }
    
    objects[newKey] = object;
    return newKey;
}

- (BOOL) addLinkerFlag:(NSString*)newFlag
             toProject:(NSMutableDictionary*)project
     forConfigurations:(NSArray*)configIds {
    
    static NSString *KEY = @"OTHER_LDFLAGS";
    static NSString *INHERIT = @"$(OTHER_LDFLAGS)";
    
    for (NSString *configId in configIds) {
        NSMutableDictionary *config = [self findObjectById:configId
                                                 inProject:project];
        
        NSMutableDictionary *settings = config[@"buildSettings"];
        id linkerFlags = settings[KEY];
        NSMutableArray *existingFlags;
        if (linkerFlags) {
            // Either an array, or a single string
            if ([linkerFlags isKindOfClass:[NSString class]]) {
                existingFlags = [NSMutableArray arrayWithObject:linkerFlags];
            } else {
                // Already an array
                existingFlags = linkerFlags;
            }
            
            BOOL flagFound = NO;
            for (NSString *flag in existingFlags) {
                if ([flag isEqualToString:newFlag]) {
                    flagFound = YES;
                    break;
                }
            }
            
            // Add, if necessary
            if (!flagFound) {
                [existingFlags addObject:newFlag];
                settings[KEY] = existingFlags;
            } // Else: already there
        } else {
            
            // Not found - add it, but inherit from project
            settings[KEY] = [NSMutableArray arrayWithObjects:newFlag, INHERIT, nil];
        }
    }
    return YES;
    
}

#pragma mark - Tasks

// SHTaskCopyClang
- (BOOL) copyClang:(BOOL)resolutionPhase {
    if (resolutionPhase) {
        // Will do this later... skip
        return YES;
    }

    // Clang binary
    NSString *clangSrc = [[NSBundle mainBundle] pathForResource:@"clang-detect"
                                                         ofType:@""
                                                    inDirectory:@"Resources/Tools/bin/"];
    NSString *dest = [self.projectFilePath stringByDeletingLastPathComponent];
    dest = [dest stringByAppendingPathComponent:@"Resources/Tools/bin/"];
    dest = [dest stringByAppendingPathComponent:clangSrc.lastPathComponent];

    NSError *error;
    BOOL success = [self copyFileFrom:clangSrc
                                   to:dest
                          isDirectory:NO
                            overwrite:YES
                                error:&error];
    if (!success) {
        return NO;
    }

    // Clang headers: ../, and remove filename
    dest = [[dest stringByDeletingLastPathComponent] stringByDeletingLastPathComponent];
    dest = [dest stringByAppendingPathComponent:@"lib"];
    NSString *libFolder = [[[NSBundle mainBundle] resourcePath]
                           stringByAppendingPathComponent:@"Resources/Tools/lib"];
    success = [self copyFileFrom:libFolder
                              to:dest
                     isDirectory:YES
                       overwrite:YES
                           error:&error];
    if (!success) {
        return NO;
    }

    // ARC libs
    NSString *pathToLibs = @"/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/arc";
    dest = [dest stringByAppendingPathComponent:@"arc"];
    success = [self copyFileFrom:pathToLibs
                              to:dest
                     isDirectory:YES
                       overwrite:YES
                           error:&error];
    if (!success) {
        return NO;
    }

    return YES;
}

// SHTaskCC
- (BOOL) setCC:(NSMutableDictionary*)debugConfiguration resolution:(BOOL)resolve {
    NSMutableDictionary *buildSettings = debugConfiguration[@"buildSettings"];
    
    NSString *pathToClang = [[self.projectFilePath stringByDeletingLastPathComponent]
                             stringByAppendingPathComponent:@"Resources/Tools/bin/"];
    if (resolve) {
        pathToClang = [pathToClang stringByAppendingPathComponent:@"clang-resolve"];
    } else {
        pathToClang = [pathToClang stringByAppendingPathComponent:@"clang-detect"];
    }
    
    NSString *currentValue = buildSettings[@"CC"];
    if (!currentValue) {
        // Not set yet
        buildSettings[@"CC"] = pathToClang;
    } else {
        if ([currentValue rangeOfString:@"clang-detect"].location != NSNotFound ||
            [currentValue rangeOfString:@"clang-resolve"].location != NSNotFound) {
            // We only overwrite it if the current value is something known/
            // most-likely set by this tools
            buildSettings[@"CC"] = pathToClang;
        } else {
            NSLog(@"'CC' build setting is already in place - it won't be overwritten automatically. You can set it to '%@' manually.", pathToClang);
        }
    }
    return YES;
}

// SHTaskObjC
- (BOOL) setObjC:(NSMutableArray*)allConfigurationIds in:(NSMutableDictionary*)project {
    
    static NSString *OBJC = @"-ObjC";
    return [self addLinkerFlag:OBJC
                     toProject:project
             forConfigurations:allConfigurationIds];
}

// SHTaskCopyFolder
- (BOOL) copyResourcesFolder {
    NSString *resourcesFolderPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Resources"];
    NSString *destination = [[self.projectFilePath stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"Resources"];
    
    NSError *error;
    BOOL success = [self copyFileFrom:resourcesFolderPath
                                   to:destination
                          isDirectory:YES
                            overwrite:YES
                                error:&error];
    if (!success) {
        return NO;
    }
    
    // Add ARC libs
    NSString *pathToLibs = @"/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/arc";
    NSString *arcDestination = [destination stringByAppendingPathComponent:@"Tools/lib/arc"];
    success = [self copyFileFrom:pathToLibs
                              to:arcDestination
                     isDirectory:YES
                       overwrite:YES
                           error:&error];
    if (!success) {
        return NO;
    }
    
    // Remove unused Clang
    NSString *pathToClangDetect = [destination stringByAppendingPathComponent:@"Tools/bin/clang-detect"];
    [[NSFileManager defaultManager] removeItemAtPath:pathToClangDetect
                                               error:&error];
    if (error) {
        NSLog(@"Error removing unused version of Clang: %@", error);
        return NO;
    }
    return YES;
}

// SHTaskAddPhases
- (BOOL) addBuildPhases:(NSDictionary*)appTarget of:(NSMutableDictionary*)project {
    NSMutableArray *buildPhaseIds = appTarget[@"buildPhases"];
    
    // We need the index of the Compile Sources phase:
    // Insert scripts before and afterwards
    int i = 0;
    int count = 0;
    BOOL found = NO;
    
    // Avoid duplicates
    BOOL hasCheckerPhase = NO;
    BOOL hasResolutionPhase = NO;
    NSString *buildActionMask;
    for (NSString *phaseId in buildPhaseIds) {
        NSDictionary *buildPhase = [self findObjectById:phaseId
                                              inProject:project];
        if ([buildPhase[@"isa"] isEqualToString:@"PBXSourcesBuildPhase"]) {
            found = YES;
            count = i;
            buildActionMask = buildPhase[@"buildActionMask"];
        } else if ([buildPhase[@"isa"] isEqualToString:@"PBXShellScriptBuildPhase"]) {
            
            // Avoid duplicates
            NSString *shellCode = buildPhase[@"shellScript"];
            if ([shellCode.lastPathComponent isEqualToString:@"xcodeprojectcheck"]) {
                hasCheckerPhase = YES;
            } else if ([shellCode.lastPathComponent isEqualToString:@"unsafeApiResolver"]) {
                hasResolutionPhase = YES;
            }
        }
        i++;
    }
    if (!found) return NO;
    if (hasCheckerPhase && hasResolutionPhase) return YES;
    
    // Create two new phases
    NSString *code = [[self.projectFilePath stringByDeletingLastPathComponent]
                      stringByAppendingPathComponent:@"Resources/Tools/bin/xcodeprojectcheck"];
    // Escape spaces
    code = [code stringByReplacingOccurrencesOfString:@" " withString:@"\\ "];

    NSDictionary *checkerPhase = @{@"isa":@"PBXShellScriptBuildPhase",
                                   @"buildActionMask":buildActionMask,
                                   @"files":@[],
                                   @"inputPaths":@[],
                                   @"outputPaths":@[],
                                   @"runOnlyForDeploymentPostProcessing":@"0",
                                   @"shellPath":@"/bin/sh",
                                   @"shellScript":code};
    NSString *checkerPhaseId = [self addObjectToProject:checkerPhase project:project];
    code = [[self.projectFilePath stringByDeletingLastPathComponent]
                stringByAppendingPathComponent:@"Resources/Tools/bin/unsafeApiResolver"];
    // Escape spaces
    code = [code stringByReplacingOccurrencesOfString:@" " withString:@"\\ "];
    NSDictionary *resolutionPhase = @{@"isa":@"PBXShellScriptBuildPhase",
                                      @"buildActionMask":buildActionMask,
                                      @"files":@[],
                                      @"inputPaths":@[],
                                      @"outputPaths":@[],
                                      @"runOnlyForDeploymentPostProcessing":@"0",
                                      @"shellPath":@"/bin/sh",
                                      @"shellScript":code};
    NSString *resPhaseId = [self addObjectToProject:resolutionPhase project:project];
    
    // Surround Compile Sources Phase with these phases
    if ( (count+1) < buildPhaseIds.count) {
        [buildPhaseIds insertObject:resPhaseId atIndex:count+1];
    } else {
        // Compile phase is the last current phase - append new phase
        [buildPhaseIds addObject:resPhaseId];
    }
    [buildPhaseIds insertObject:checkerPhaseId atIndex:count];
    
    return YES;
}

// SHTaskAddSubproj
- (BOOL) addSubproject:(NSMutableDictionary*)project {
    // First, move files into place
    NSString *directory = [[self.projectFilePath stringByDeletingLastPathComponent]
                           stringByAppendingPathComponent:@"Resources/Templates/SHManagedCompatibilityLibrary"];
    NSString *destination = [[self.projectFilePath stringByDeletingLastPathComponent]
                             stringByAppendingPathComponent:@"SHManagedCompatibilityLibrary"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:destination]) {
        // Avoid duplicates
        return YES;
    }
    
    // NEVER overwrite the project, as it may include handwritten code
    NSError *error;
    BOOL success = [self copyFileFrom:directory
                                   to:destination
                          isDirectory:YES
                            overwrite:NO
                                error:&error];
    if (!success) {
        return NO;
    }
    // Cleanup
    [[NSFileManager defaultManager] removeItemAtPath:directory error:&error];
    if (error) {
        NSLog(@"Failed removing the library project from the templates folder.");
        return NO;
    }

    // Load subproject's projectfile
    NSString *subProjectFilePath = [[self.projectFilePath stringByDeletingLastPathComponent]
                                    stringByAppendingPathComponent:@"SHManagedCompatibilityLibrary/SHManagedCompatibilityLibrary.xcodeproj/project.pbxproj"];
    NSDictionary *subProject = [NSDictionary dictionaryWithContentsOfFile:subProjectFilePath];
    // Path: Project -> Static Library Target -> productReference
    NSMutableDictionary *target = [self findLibraryTarget:subProject idOnly:NO];
    NSString *productRefId = target[@"productReference"];
    
    // Add subproject
    NSString *rootObjectId = project[@"rootObject"];
    NSMutableDictionary *xcodeProject = [self findObjectById:rootObjectId inProject:project];
    NSMutableArray *projectRefs = xcodeProject[@"projectReferences"];
    
    // Subproject file reference
    NSDictionary *fileRef = @{@"isa":@"PBXFileReference",
                              @"lastKnownFileType":@"wrapper.pb-project",
                              @"name":@"SHManagedCompatibilityLibrary.xcodeproj",
                              @"path":@"SHManagedCompatibilityLibrary/SHManagedCompatibilityLibrary.xcodeproj",
                              @"sourceTree":@"<group>"};
    NSString *fileRefId = [self addObjectToProject:fileRef project:project];
    
    // Add to group
    NSString *mainGroupId = xcodeProject[@"mainGroup"];
    NSMutableDictionary *mainGroup = [self findObjectById:mainGroupId inProject:project];
    NSMutableArray *children = mainGroup[@"children"];
    [children insertObject:fileRefId atIndex:0];
    
    // Container Proxy: Points to product of subproject
    NSDictionary *containerProxy = @{@"isa":@"PBXContainerItemProxy",
                                     @"containerPortal":fileRefId,
                                     @"proxyType":@"2",
                                     @"remoteGlobalIDString":productRefId,
                                     @"remoteInfo":@"SHManagedCompatibilityLibrary"};
    NSString *containerProxyId = [self addObjectToProject:containerProxy
                                                  project:project];
    
    // Reference Proxy: Forwards to .a file
    NSDictionary *refProxy = @{@"isa":@"PBXReferenceProxy",
                               @"fileType":@"archive.ar",
                               @"path":@"libSHManagedCompatibilityLibrary.a",
                               @"remoteRef":containerProxyId,
                               @"sourceTree":@"BUILT_PRODUCTS_DIR"};
    NSString *refProxyId = [self addObjectToProject:refProxy
                                            project:project];

    // New products group
    NSDictionary *productsGroup = @{@"isa":@"PBXGroup",
                                    @"children":@[refProxyId],
                                    @"name":@"Products",
                                    @"sourceTree":@"<group>"};
    NSString *productsGroupId = [self addObjectToProject:productsGroup
                                                 project:project];
    
    NSDictionary *subProjectRef = @{@"ProductGroup":productsGroupId,
                                    @"ProjectRef":fileRefId};
    
    if (projectRefs) {
        [projectRefs addObject:subProjectRef];
    } else {
        xcodeProject[@"projectReferences"] = @[subProjectRef];
    }

    // Add Target dependency
    NSDictionary *targetIdDict = [self findLibraryTarget:subProject idOnly:YES];
    NSDictionary *targetContainerProxy = @{@"isa":@"PBXContainerItemProxy",
                                           @"containerPortal":fileRefId,
                                           @"proxyType":@"1",
                                           @"remoteGlobalIDString":targetIdDict[@"targetId"],
                                           @"remoteInfo":@"SHManagedCompatibilityLibrary"};
    NSString *targetContainerProxyId = [self addObjectToProject:targetContainerProxy
                                                        project:project];
    NSDictionary *targetDependency = @{@"isa":@"PBXTargetDependency",
                                       @"name":@"SHManagedCompatibilityLibrary",
                                       @"targetProxy":targetContainerProxyId};
    NSString *targetDependencyId = [self addObjectToProject:targetDependency
                                                    project:project];
    NSMutableDictionary *appTarget = [self findAppTarget:project idOnly:NO];
    NSMutableArray *dependencies = appTarget[@"dependencies"];
    if (!dependencies) {
        appTarget[@"dependencies"] = @[targetDependencyId];
    }
    [dependencies addObject:targetDependencyId];

    // Link phase
    // Affected: FrameworksBuildPhase - [+] BuildFile -> Products Group
    /*NSDictionary *buildFile = @{@"isa":@"PBXBuildFile",
                                @"fileRef":productsGroupId};
    NSString *buildFileId = [self addObjectToProject:buildFile
                                             project:project];
    NSMutableArray *buildPhases = appTarget[@"buildPhases"];
    NSMutableDictionary *fwBuildPhase;
    for (NSString *buildPhaseId in buildPhases) {
        NSMutableDictionary *buildPhase = [self findObjectById:buildPhaseId
                                                     inProject:project];
        NSString *isaString = buildPhase[@"isa"];
        if ([isaString isEqualToString:@"PBXFrameworksBuildPhase"]) {
            fwBuildPhase = buildPhase;
            break;
        }
    }
    if (!fwBuildPhase) {
        NSLog(@"Abort: Frameworks Build Phase not found.");
        return NO;
    }
    NSMutableArray *fwPhaseFiles = fwBuildPhase[@"files"];
    [fwPhaseFiles addObject:buildFileId]; // Wrong file: This somehow links the wrong target (the app) */
    // TODO: Clean solution with link phase
    
    // Add linker flag - temporary solution
    static NSString *LINK_FLAG = @"\"$(BUILT_PRODUCTS_DIR)/libSHManagedCompatibilityLibrary.a\"";
    
    NSString *buildConfigsId = appTarget[@"buildConfigurationList"];
    NSMutableDictionary *buildConfigs = [self findObjectById:buildConfigsId inProject:project];
    NSMutableArray *allTargetConfigsIds = buildConfigs[@"buildConfigurations"];
    success = [self addLinkerFlag:LINK_FLAG
                        toProject:project
                forConfigurations:allTargetConfigsIds];
    if (!success) {
        NSLog(@"Error adding flag to link compatibility library.");
        return NO;
    }

    return YES;
}

// SHTaskImportHeader
- (BOOL) importHeader:(NSMutableDictionary*)project {
    static NSString *importCheck = @"#import \"SHManagedCompatibilityLibrary/SHManagedCompatibilityLibrary.h\"";

    // Load the import and suppression macros
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Suppression" ofType:@"txt"];
    NSString *textToInsert = [NSString stringWithContentsOfFile:path
                                                              encoding:NSUTF8StringEncoding
                                                                 error:nil];
    
    // Get the PCH name/path
    NSMutableDictionary *target = [self findAppTarget:project idOnly:NO];
    NSString *buildConfigsId = target[@"buildConfigurationList"];
    NSDictionary *buildConfigs = [self findObjectById:buildConfigsId inProject:project];
    NSArray *buildConfigsList = buildConfigs[@"buildConfigurations"];

    NSMutableDictionary *pchFiles = [NSMutableDictionary dictionaryWithCapacity:5];
    for (NSString *configId in buildConfigsList) {
        NSDictionary *buildConfig = [self findObjectById:configId
                                               inProject:project];
        NSDictionary *settings = buildConfig[@"buildSettings"];
        NSString *pchName = settings[@"GCC_PREFIX_HEADER"];
        if (pchName) {
            if (!pchFiles[pchName]) {
                pchFiles[pchName] = @YES;
            }
        }
    }

    for (NSString *file in pchFiles.allKeys) {
        // Get absolute path
        NSString *filePath = [[self.projectFilePath stringByDeletingLastPathComponent]
                stringByAppendingPathComponent:file];
        
        // Avoid duplicates
        NSError *error;
        NSString *fileContent = [NSString stringWithContentsOfFile:filePath
                                                          encoding:NSUTF8StringEncoding
                                                             error:&error];
        if (error) {
            NSLog(@"Error reading PCH file (%@): %@", filePath, error);
            return NO;
        }
        NSRange importRange = [fileContent rangeOfString:importCheck];
        if (importRange.location != NSNotFound) {
            // It's already there
            return YES;
        }
        
        NSFileHandle *file = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [file seekToEndOfFile];
        [file writeData:[textToInsert dataUsingEncoding:NSUTF8StringEncoding]];
        [file closeFile];
    }

    return YES;
}

// SHTaskSyncDeplTargets
- (BOOL) syncDeploymentTargets:(NSMutableDictionary*)project {
    // Depl Target can be defined in project or target
    // Target has priority: Only look into project if the target
    // has no own setting
    
    NSDictionary *target = [self findAppTarget:project idOnly:NO];
    NSString *configListId = target[@"buildConfigurationList"];
    NSString *deplTarget = [self deploymentTargetFromConfigurationList:configListId
                                                                     in:project];
    
    if (!deplTarget || deplTarget.length == 0) {
        NSString *xcodeProjId = project[@"rootObject"];
        NSDictionary *xcodeProj = [self findObjectById:xcodeProjId inProject:project];
        configListId = xcodeProj[@"buildConfigurationList"];
        deplTarget = [self deploymentTargetFromConfigurationList:configListId
                                                              in:project];
    }
    
    // Sync
    // Find subproject
    NSString *subProjectPath = [[self.projectFilePath stringByDeletingLastPathComponent]
                                stringByAppendingPathComponent:@"SHManagedCompatibilityLibrary/SHManagedCompatibilityLibrary.xcodeproj/project.pbxproj"];
    NSMutableDictionary *subProject = [NSMutableDictionary dictionaryWithContentsOfFile:subProjectPath];
    // All elements are now mutable
    subProject = CFBridgingRelease(CFPropertyListCreateDeepCopy(NULL,
                                                             (__bridge CFPropertyListRef)(subProject),
                                                             kCFPropertyListMutableContainersAndLeaves));
    NSMutableDictionary *subTarget = [self findLibraryTarget:subProject idOnly:NO];
    configListId = subTarget[@"buildConfigurationList"];
    NSMutableDictionary *configList = [self findObjectById:configListId inProject:subProject];
    NSArray *configIds = configList[@"buildConfigurations"];
    for (NSString *configId in configIds) {
        NSMutableDictionary *configuration = [self findObjectById:configId inProject:subProject];
        NSMutableDictionary *settings = configuration[@"buildSettings"];
        settings[@"IPHONEOS_DEPLOYMENT_TARGET"] = deplTarget;
    }
    
    // Write back
    return [subProject writeToFile:subProjectPath atomically:YES];
}

// SHTaskSearchPaths
- (BOOL) addSearchPaths:(NSMutableDictionary*)project
              toConfigs:(NSMutableArray*)configurationIds {
    static NSString *USER_SEARCH_PATH_KEY = @"USER_HEADER_SEARCH_PATHS";
    static NSString *INHERIT = @"$(USER_HEADER_SEARCH_PATHS)";
    static NSString *PATH = @"\"$(SRCROOT)/SHManagedCompatibilityLibrary/\"";
    
    for (NSString *configId in configurationIds) {
        NSDictionary *configuration = [self findObjectById:configId inProject:project];
        NSMutableDictionary *settings = configuration[@"buildSettings"];
        id searchPaths = settings[USER_SEARCH_PATH_KEY];
        if (searchPaths) {
            NSMutableArray *existingPaths = searchPaths;
            if ([searchPaths isKindOfClass:[NSString class]]) {
                existingPaths = [NSMutableArray arrayWithObject:searchPaths];
            }
            
            // Already in there?
            BOOL exists = NO;
            for (NSString *path in existingPaths) {
                if ([path isEqualToString:PATH]) {
                    //exists = YES;
                    return YES;
                }
            }
            if (!exists) {
                [existingPaths addObject:PATH];
                settings[USER_SEARCH_PATH_KEY] = existingPaths;
            }
            
        } else {
            // Add new - inherit existing
            settings[USER_SEARCH_PATH_KEY] = @[INHERIT, PATH];
        }
    }
    return YES;
}

// SHTaskExportFlag
- (BOOL) setExportFlag:(NSDictionary*)debugConfig {
    // Debug only
    static NSString *EXPORT_FLAG = @"-export-unsafe-api-usage";
    static NSString *FLAG = @"OTHER_CFLAGS";
    static NSString *INHERIT = @"$(OTHER_CFLAGS)";
    
    NSMutableDictionary *settings = debugConfig[@"buildSettings"];
    id cFlags = settings[FLAG];
    if (cFlags) {
        NSMutableArray *existingFlags = cFlags;
        if ([cFlags isKindOfClass:[NSString class]]) {
            existingFlags = [NSMutableArray arrayWithObject:cFlags];
        }
        
        // Already in there?
        BOOL exists = NO;
        for (NSString *flag in existingFlags) {
            if ([flag isEqualToString:EXPORT_FLAG]) {
                //exists = YES;
                return YES;
            }
        }
        if (!exists) {
            [existingFlags addObject:EXPORT_FLAG];
            settings[FLAG] = existingFlags;
        }
        
    } else {
        // Add new - inherit existing
        settings[FLAG] = @[INHERIT, EXPORT_FLAG];
    }
    
    return YES;
}

@end
