//
//  SHAppDelegate.m
//  Unsafe API Usage Tool Installer
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import "SHAppDelegate.h"
#import "SHSelectionViewController.h"

@interface SHAppDelegate()

@property (weak) IBOutlet NSView *view;
@property (weak) IBOutlet SHDroppableImageView *imageView;
@property (weak) IBOutlet NSTextField *projectNameLabel;
@property (weak) IBOutlet NSButton *continueButton;
@property (weak) IBOutlet NSView *contentView;

@property (strong) NSString *filePath;
@property (strong) SHSelectionViewController *currentController;

@end

@implementation SHAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    self.imageView.delegate = self;
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender {
    return YES;
}

#pragma mark - This serves as the first view controller

- (void)fileHasBeenDropped:(NSString *)filePath {
    self.projectNameLabel.hidden = NO;
    self.projectNameLabel.stringValue = [filePath lastPathComponent];
    self.filePath = filePath;
    self.imageView.image = [NSImage imageNamed:@"xcode_proj_logo"];
    self.continueButton.enabled = YES;
}

- (void)fileHasBeenRemoved {
    self.projectNameLabel.hidden = YES;
    self.imageView.image = [NSImage imageNamed:@"xcode_proj_logo_trans"];
    self.continueButton.enabled = NO;
}

- (IBAction)continueWithSelection:(id)sender {
    // Load next view (controller)
    SHSelectionViewController *selCtr = [[SHSelectionViewController alloc]
                                         initWithNibName:@"SelectionView"
                                         bundle:nil];
    selCtr.projectFilePath = self.filePath;

    NSView *topView = selCtr.view;
    self.currentController = selCtr;

    // Resize the window
    float topBarHeight = self.window.frame.size.height - self.view.frame.size.height;
    NSRect newFrame = NSRectFromCGRect(CGRectMake(self.window.frame.origin.x,
                                                  self.window.frame.origin.y,
                                                  topView.frame.size.width,
                                                  topView.frame.size.height +  topBarHeight));

    NSRect topViewRect = topView.frame;
    //topViewRect.origin = CGPointMake(-windowSize.width, 0);
    topView.frame = topViewRect;
    topView.alphaValue = 0.0;

    [NSAnimationContext runAnimationGroup:^(NSAnimationContext *context) {
        // 1st: Fade out existing view
        [[self.contentView animator] setAlphaValue:0.0];

    } completionHandler:^{

        // 2nd: Resize window
        [self.window setFrame:newFrame display:YES animate:YES];
        [self.view addSubview:topView];

        [NSAnimationContext runAnimationGroup:^(NSAnimationContext *context) {
            // 3rd: Show new view
            [[topView animator] setAlphaValue:1.0];

        } completionHandler:^{
            // Clean up
            [self.contentView removeFromSuperview];
        }];
    }];
}

@end
