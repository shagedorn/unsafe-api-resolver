//
//  SHDroppableImageView.m
//  Unsafe API Usage Tool Installer
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import "SHDroppableImageView.h"

@interface SHDroppableImageView ()

@property (strong) NSString *currentFileName;

@end

@implementation SHDroppableImageView

- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender {

    NSPasteboard *pboard = [sender draggingPasteboard];
    NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
    if (files.count != 1) {
        // One at a time
        return NSDragOperationNone;
    }

    // We know there's exactly 1 item
    NSString *filename = files[0];
    NSString *extension = [filename pathExtension];
    if ([extension isEqualToString:@"xcodeproj"]) {
        self.currentFileName = filename;
        return NSDragOperationCopy;
    }
    return NSDragOperationNone;
}

- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender {
    // We do checks before - we know we get a project file
    [self.delegate fileHasBeenDropped:self.currentFileName];
    return YES;
}

- (BOOL)allowsCutCopyPaste {
    return NO;
}



@end
