

#import "SHManagedCompatibilityLibrary/SHManagedCompatibilityLibrary.h"

// Macros to suppress unsafe API warnings
#ifndef __SUPPRESS_FIX_BEGIN__
#define __SUPPRESS_FIX_BEGIN__ _Pragma("clang diagnostic push") \
                               _Pragma("clang diagnostic ignored \"-Wfix-unsafe-api-usage\"")
#define __SUPPRESS_FIX_END__   _Pragma("clang diagnostic pop")
#endif

#ifndef __SUPPRESS_CRITICAL_API_BEGIN__
#define __SUPPRESS_CRITICAL_API_BEGIN__ _Pragma("clang diagnostic push") \
                                        _Pragma("clang diagnostic ignored \"-Wfix-unsafe-api-usage\"")
#define __SUPPRESS_CRITICAL_API_END__   _Pragma("clang diagnostic pop")
#endif

#ifndef __SUPPRESS_UNFIXABLE_CRITICAL_API_BEGIN__
#define __SUPPRESS_UNFIXABLE_CRITICAL_API_BEGIN__ _Pragma("clang diagnostic push") \
                                                  _Pragma("clang diagnostic ignored \"-Wfix-unsafe-api-usage\"")
#define __SUPPRESS_UNFIXABLE_CRITICAL_API_END__   _Pragma("clang diagnostic pop")
#endif

#ifndef __SUPPRESS_ALL_UNSAFE_API_BEGIN__
#define __SUPPRESS_ALL_UNSAFE_API_BEGIN__ __SUPPRESS_UNFIXABLE_CRITICAL_API_BEGIN__ \
                                          __SUPPRESS_CRITICAL_API_BEGIN__ \
                                          __SUPPRESS_FIX_BEGIN__
#define __SUPPRESS_ALL_UNSAFE_API_END__ __SUPPRESS_UNFIXABLE_CRITICAL_API_END__ \
                                        __SUPPRESS_CRITICAL_API_END__ \
                                        __SUPPRESS_FIX_END__
#endif
