//
//  SHDroppableImageView.h
//  Unsafe API Usage Tool Installer
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import <Cocoa/Cocoa.h>

/**
 *  Notification when drop was successful
 */
@protocol SHFileDropDelegate <NSObject>

- (void) fileHasBeenRemoved;
- (void) fileHasBeenDropped:(NSString*)filePath;

@end

@interface SHDroppableImageView : NSImageView

@property (weak) id<SHFileDropDelegate> delegate;

@end
