//
//  SHAppDelegate.h
//  Unsafe API Usage Tool Installer
//
//  Created by Sebastian Hagedorn
//  Copyright (c) 2013 Sebastian Hagedorn. All rights reserved.
//
//  See LICENSE.md in the root directory of the repository for
//  a license.
//

#import <Cocoa/Cocoa.h>
#import "SHDroppableImageView.h"

@interface SHAppDelegate : NSObject <NSApplicationDelegate, SHFileDropDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
