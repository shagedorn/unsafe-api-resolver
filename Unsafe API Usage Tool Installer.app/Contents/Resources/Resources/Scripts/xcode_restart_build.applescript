-- The script looks for an open Xcode project window with a
-- given project name. If found, it stops the current build
-- process and restarts it.


-- These vars have to be overwritten programmatically!

set projectName to "###PROJECT_NAME###"
set buildAction to "###BUILD_ACTION###"


-- Start interacting with Xcode
tell application "Xcode"
	
	-- Make sure to operate on the correct window
	repeat with i from 1 to number of items in windows
		set this_window to item i of windows
		
		if the name of this_window starts with projectName then
			
			tell this_window to activate
			
			-- Stop the current build
			tell application "System Events" to tell process ¬
				"Xcode" to click menu item ¬
				"Stop" of menu 1 ¬
				of menu bar item ¬
				"Product" of menu bar 1
			
			-- Rebuild
			tell application "System Events" to tell process ¬
				"Xcode" to click menu item ¬
				buildAction of menu 1 ¬
				of menu bar item ¬
				"Product" of menu bar 1
			
			-- We assume there's only 1 hit anyway
			exit repeat
		end if
	end repeat
	
end tell