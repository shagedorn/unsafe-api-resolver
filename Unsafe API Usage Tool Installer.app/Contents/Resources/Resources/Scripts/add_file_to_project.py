##
##  Adds all files from a given source directory
##  to a project.
##

# Static settings
group_name = 'SHManagedCompatibilityLibrary'

# Make sure the module can be found
import sys
project_base_path = sys.argv[1]
sys.path.append(project_base_path+'/../Resources/Tools/mod-pbxproj')

from mod_pbxproj import XcodeProject

project = XcodeProject.Load(project_base_path+'/SHManagedCompatibilityLibrary.xcodeproj/project.pbxproj')

# Some files comes with the project template
project.add_folder(project_base_path+'/SHManagedCompatibilityLibrary', excludes=['^.*\.pch$','^.*\.xcodeproj$', '^.*\.plist$', 'SHManagedCompatibilityLibrary.h', 'SHLibraryHelper.m', 'SHLibraryHelper.h'])

if project.modified:
    #project.backup()
    project.saveFormat3_2() #IMPORTANT, DONT USE THE OLD VERSION!
