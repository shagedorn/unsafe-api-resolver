    Class nativeClass = [SHManaged###ORIGINAL_CLASS### nativeClassOrNil];
    if (nativeClass) {
        #warning decide whether you want to use the native implementation, if available
        [nativeClass ###SEL_AND_ARGS###];
        //[super ###SEL_AND_ARGS###];
        return;
    }

    // Call super or provide custom fallback implementation
    [super ###SEL_AND_ARGS###];