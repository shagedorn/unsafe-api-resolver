    if (self.nativeImpl) {
        #warning decide whether you want to use the native implementation, if available
        return ###CAST###[self.nativeImpl ###SEL_AND_ARGS###];
        //return [super ###SEL_AND_ARGS###];
    }

    // Call super or provide custom fallback implementation
    return [super ###SEL_AND_ARGS###];