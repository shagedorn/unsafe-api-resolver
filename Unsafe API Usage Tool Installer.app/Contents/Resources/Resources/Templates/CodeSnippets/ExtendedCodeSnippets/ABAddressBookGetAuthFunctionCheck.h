    if (###SYMBOL_NAME### != NULL) {
        // The function exists on this iOS version
        // Use the native implementation
        return ###SYMBOL_NAME###(###SYMBOL_ARGS###);
    } else {
        // The above function does not exist on this iOS version
        // Authorisation is not necessary until iOS 6
        return kABAuthorizationStatusAuthorized;
    }