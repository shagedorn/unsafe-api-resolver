    if (###SYMBOL_NAME### != NULL) {
        // The function exists on iOS 6+
        // Use the native implementation
        return ###SYMBOL_NAME###(###SYMBOL_ARGS###);
    } else {
        // The original function does not exist prior to iOS 6
        // However, this one does the same job... (on iOS 5)
        return ABAddressBookCreate();
    }