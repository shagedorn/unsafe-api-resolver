    /*
     *  This block ensures to prioritise the native
     *  implementation
     */
    if (self.nativeImpl) {
        [self.nativeImpl ###SEL_AND_ARGS###];
        return;
    }
    #warning Missing fallback implementation