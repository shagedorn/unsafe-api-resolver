//
//  SHManaged###ORIGINAL_CLASS###.m
//
//  Created by Unsafe API Resolver.
//  https://bitbucket.org/shagedorn/unsafe-api-resolver
//

#import "SHManaged###ORIGINAL_CLASS###.h"

/**
 *  Private Interface
 */

// Pointer to the class itself
static Class _nativeClass = nil;
static BOOL _nativeClassHasBeenChecked = NO;

@interface SHManaged###ORIGINAL_CLASS### ()

// Pointer to an instance of the original class
@property (strong, nonatomic) ###ORIGINAL_CLASS### *nativeImpl;
@property (assign) BOOL nativeImplHasBeenChecked;

@end

@implementation SHManaged###ORIGINAL_CLASS###

- (###ORIGINAL_CLASS### *) nativeImpl {
    if (!_nativeImpl && !self.nativeImplHasBeenChecked) {
        Class nativeClass = NSClassFromString(@"###ORIGINAL_CLASS###");
        if (nativeClass) {
            self.nativeImpl = [nativeClass new];
        }
        /*
         *  Don't check class availability more than once
         */
        self.nativeImplHasBeenChecked = YES;
    }
    return _nativeImpl;
}

+ (Class) nativeClassOrNil {
    if (!_nativeClass && !_nativeClassHasBeenChecked) {
        Class nativeClass = NSClassFromString(@"###ORIGINAL_CLASS###");
        _nativeClass = nativeClass;
        _nativeClassHasBeenChecked = YES;
    }
    return _nativeClass;
}

/**
 *  Do not remove any of the following implemenations, as
 *  your application could crash on legacy systems.
 */
#pragma mark - Generated methods for backwards compatibility

/**
 *  Not implementing these method does not result in a crash,
 *  as they are declared by a superclass. The
 *  superclass' implementation may not be the desired
 *  implementation to mock ###ORIGINAL_CLASS###, though.
 */
#pragma mark - Methods that ###ORIGINAL_CLASS### might overwrite

@end
