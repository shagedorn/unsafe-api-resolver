//
//  SHLibraryHelper.m
//  SHManagedCompatibilityLibrary
//
//  Created by Unsafe API Resolver.
//  https://bitbucket.org/shagedorn/unsafe-api-resolver
//

#import "SHLibraryHelper.h"
#import <objc/runtime.h>
#import <objc/message.h>

@implementation SHLibraryHelper

+ (SEL)createManagedVersionOfSelector:(SEL)criticalSelector {
    NSString *orig = NSStringFromSelector(criticalSelector);
    NSString *firstChar = [orig substringToIndex:1];
    firstChar = firstChar.uppercaseString;
    orig = [orig stringByReplacingCharactersInRange:NSMakeRange(0, 1)
                                         withString:firstChar];
    return NSSelectorFromString([@"SHManaged" stringByAppendingString:orig]);
}

#import <objc/runtime.h>
#import <objc/message.h>
//....

+ (void) swizzleOriginalSel:(SEL)origSelector
                    withSel:(SEL)managedSelector
                   forClass:(Class)theClass {

    Method origMethod = class_getInstanceMethod(theClass, origSelector);
    Method newMethod = class_getInstanceMethod(theClass, managedSelector);
    if(class_addMethod(theClass,
                       origSelector,
                       method_getImplementation(newMethod),
                       method_getTypeEncoding(newMethod))) {
        class_replaceMethod(theClass,
                            managedSelector,
                            method_getImplementation(origMethod),
                            method_getTypeEncoding(origMethod));
    }
    else {
        method_exchangeImplementations(origMethod, newMethod);
    }
}

@end
