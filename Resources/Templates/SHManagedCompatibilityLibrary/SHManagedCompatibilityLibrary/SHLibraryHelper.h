//
//  SHLibraryHelper.h
//  SHManagedCompatibilityLibrary
//
//  Created by Unsafe API Resolver.
//  https://bitbucket.org/shagedorn/unsafe-api-resolver
//

#import <Foundation/Foundation.h>

@interface SHLibraryHelper : NSObject

/**
 *  Adds a prefix: 'SHManaged', capitalises the first
 *  letter of the original selector.
 */
+ (SEL) createManagedVersionOfSelector:(SEL)criticalSelector;

/**
 *  Make sure you understand the concept and dangers of
 *  method swizzling before using it. The implementation
 *  is taken from this source:
 *
 *  http://cocoadev.com/wiki/MethodSwizzling
 *
 *  The concept/dangers are nicely described here:
 *
 *  http://stackoverflow.com/questions/5339276/what-are-the-dangers-of-method-swizzling-in-objective-c
 *
 */
+ (void) swizzleOriginalSel:(SEL)origSelector
                    withSel:(SEL)managedSelector
                   forClass:(Class)theClass;

@end
