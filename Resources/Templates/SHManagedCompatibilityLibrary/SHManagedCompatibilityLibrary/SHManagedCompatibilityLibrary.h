//
//  SHManagedCompatibilityLibrary.h
//
//  Created by Unsafe API Resolver.
//  https://bitbucket.org/shagedorn/unsafe-api-resolver
//
//  This file contains an automatically updating list
//  of generated headers. The target project should
//  only import this file.
//

/**
 *  Framework Imports: Import them here as the .pch file
 *  is generated differently for plain C files which
 *  may lead to build errors
 */

#ifdef __OBJC__
    #import <Foundation/Foundation.h>
#endif

/**
 *  Library files: Make these header files available
 *  to projects that use the library.
 */

#import "SHManagedCompatibilityLibrary/SHLibraryHelper.h"
