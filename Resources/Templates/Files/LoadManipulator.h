//
//  ###CONTAINER_CLASS###+SHManaged.h
//
//  Created by Unsafe API Resolver.
//  https://bitbucket.org/shagedorn/unsafe-api-resolver
//
//  IMPORTANT NOTE: Do not change existing documentation.
//  It is managed and updated by the Unsafe API Resolver tool.
//
//  You may append or prepend other information.
//

/**
 *  This is redundant, but helps for correct syntax
 *  highlighting in plain C files.
 */
#import <Foundation/Foundation.h>

@interface ###CONTAINER_CLASS### (SHManaged)

@end
