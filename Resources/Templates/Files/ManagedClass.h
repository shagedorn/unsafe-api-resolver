//
//  SHManaged###ORIGINAL_CLASS###.h
//
//  Created by Unsafe API Resolver.
//  https://bitbucket.org/shagedorn/unsafe-api-resolver
//
//  IMPORTANT NOTE: Do not change existing documentation.
//  It is managed and updated by the Unsafe API Resolver tool.
//
//  You may append or prepend other information.
//

// This class is meant as a replacement for ###ORIGINAL_CLASS###
// which may not be present on every device you intend to support.
//
// Do not subclass ###ORIGINAL_CLASS### as it will not be
// available when SHManaged###ORIGINAL_CLASS### is actually
// needed.

// This is not a complete mirror of ###ORIGINAL_CLASS###'s
// interface, but only the subset which is used by the application.
@interface SHManaged###ORIGINAL_CLASS### : NSObject


@end
