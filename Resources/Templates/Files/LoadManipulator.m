//
//  ###CONTAINER_CLASS###+SHManaged.m
//
//  Created by Unsafe API Resolver.
//  https://bitbucket.org/shagedorn/unsafe-api-resolver
//

#import "###CONTAINER_CLASS###+SHManaged.h"
#import "objc/runtime.h"
#import "SHLibraryHelper.h"

@implementation ###CONTAINER_CLASS### (SHManaged)

/**
 *  Generated code
 */

/** Load does not overwrite anything - all categories'
 *  +load implementations are called
 *  --> No race condition/overwriting
 */
+ (void)load {
    // TODO: method swizzling
}

/**
 *  This helps Unsafe API Resolver to detect certain locations
 *  in the generated code. Do not use this definition in your
 *  own code.
 */
#define NO_SELECTOR_YET NO

+ (BOOL)resolveInstanceMethod:(SEL)sel {
    if (NO_SELECTOR_YET)
    {
        SEL fallbackSel =  [SHLibraryHelper createManagedVersionOfSelector:sel];
        Method fallbackM = class_getInstanceMethod([self class], fallbackSel);
        const char *types = method_getTypeEncoding(fallbackM);
        IMP impl = method_getImplementation(fallbackM);
        class_addMethod([self class], sel, impl, types);

        return YES;
    }
    return [super resolveInstanceMethod:sel];
}

+ (BOOL)resolveClassMethod:(SEL)sel {
    if (NO_SELECTOR_YET)
    {
        SEL fallbackSel =  [SHLibraryHelper createManagedVersionOfSelector:sel];
        Method fallbackM = class_getClassMethod([self class], fallbackSel);
        const char *types = method_getTypeEncoding(fallbackM);
        IMP impl = method_getImplementation(fallbackM);
        NSString *className = NSStringFromClass([self class]);
        Class metaClass = objc_getMetaClass(className.UTF8String);
        class_addMethod(metaClass, sel, impl, types);

        return YES;
    }
    return [super resolveClassMethod:sel];
}

#pragma mark - Generated methods for backwards compatibility


@end
