    Class nativeClass = [SHManaged###ORIGINAL_CLASS### nativeClassOrNil];
    if (nativeClass) {
        #warning decide whether you want to use the native implementation, if available
        return ###CAST###[nativeClass ###SEL_AND_ARGS###];
        //return [super ###SEL_AND_ARGS###];
    }

    // Call super or provide custom fallback implementation
    return [super ###SEL_AND_ARGS###];