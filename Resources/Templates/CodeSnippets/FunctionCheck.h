    if (###SYMBOL_NAME### != NULL) {
        // The function exists on this iOS version
        // Use the native implementation
        return ###SYMBOL_NAME###(###SYMBOL_ARGS###);
    } else {
        // The function does not exist on this iOS version
        // You need to provide a fallback implementation
        #warning Missing fallback implementation
        return ###DEFAULT_RETURN###;
    }