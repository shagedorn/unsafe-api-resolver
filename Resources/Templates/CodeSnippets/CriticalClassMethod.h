    /*
     *  This block ensures to prioritise the native
     *  implementation
     */
    Class nativeClass = [SHManaged###ORIGINAL_CLASS### nativeClassOrNil];
    if (nativeClass) {
        return ###CAST###[nativeClass ###SEL_AND_ARGS###];
    }
    #warning Missing fallback implementation
    return nil;