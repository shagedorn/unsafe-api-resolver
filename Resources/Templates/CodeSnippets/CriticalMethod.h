    /*
     *  This block ensures to prioritise the native
     *  implementation
     */
    if (self.nativeImpl) {
        return ###CAST###[self.nativeImpl ###SEL_AND_ARGS###];
    }
    #warning Missing fallback implementation
    return ###DEFAULT_RETURN###;