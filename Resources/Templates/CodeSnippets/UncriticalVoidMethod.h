    if (self.nativeImpl) {
        #warning decide whether you want to use the native implementation, if available
        [self.nativeImpl ###SEL_AND_ARGS###];
        //[super ###SEL_AND_ARGS###];
        return;
    }

    // Call super or provide custom fallback implementation
    [super ###SEL_AND_ARGS###];