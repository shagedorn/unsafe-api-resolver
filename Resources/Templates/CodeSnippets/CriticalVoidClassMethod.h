    /*
     *  This block ensures to prioritise the native
     *  implementation
     */
    Class nativeClass = [SHManaged###ORIGINAL_CLASS### nativeClassOrNil];
    if (nativeClass) {
        [nativeClass ###SEL_AND_ARGS###];
        return;
    }
    #warning Missing fallback implementation