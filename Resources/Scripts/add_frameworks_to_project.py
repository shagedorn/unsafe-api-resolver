##
##  Adds all framework paths to a given project.
##  Important: Weak link frameworks!
##


# Static settings
group_name = 'SHManagedCompatibilityLibrary'

# Make sure the module can be found
import sys
project_base_path = sys.argv[1]
sys.path.append(project_base_path+'/../Resources/Tools/mod-pbxproj')

from mod_pbxproj import XcodeProject

project = XcodeProject.Load(project_base_path+'/SHManagedCompatibilityLibrary.xcodeproj/project.pbxproj')

# indices 0,1 reserved for script file name and project path
frameworkGroup = project.get_or_create_group('Frameworks')
for i in range(2, len(sys.argv)):
    libFilePath = sys.argv[i]
    project.add_file(libFilePath,
                     parent=frameworkGroup,
                     tree='SDKROOT',
                     weak=True)

if project.modified:
    #project.backup()
    project.saveFormat3_2() #IMPORTANT, DONT USE THE OLD VERSION!
